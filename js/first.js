(function ($, ts){
    console.log('PayU Card');
    let attrs = [
        'merchant-pos-id',
        'shop-name',
        'total-amount',
        'currency-code',
        'customer-language',
        'store-card',
        'recurring-payment',
        'customer-email',
        'sig',
        'widget-mode',
    ];

    document.addEventListener("DOMContentLoaded", function() {
        let wrapper = $('#content-inner').length > 0 ? $('#content-inner') : $('.region--content.region');
        console.log(wrapper);
        wrapper.append('<div id="payu-widget"></div>');

        let script = $('<script success-callback="payuOnSuccess" pay-button="#pay-button" />');
        attrs.forEach(function(name, index){
            script.attr(name, CRM.vars.payu[name]);
        })
        script.attr('src', CRM.vars.payu['url-site'] + '/front/widget/js/payu-bootstrap.js');
        script.appendTo('body');
    });
}(CRM.$, CRM.ts('eu.caltha.payu')));
/*
* Callback dla PayU
* */
window.payuOnSuccess = function($data) {
    let form = CRM.$('<form method="POST" action="/civicrm/payu/second"></form>'),
        args = {
            token : $data.value,
            maskedCard : $data.maskedCard,
            tokenType : $data.tokenType,
            type : $data.type,
            contributionID : CRM.vars.payu['contribution-id'],
            merchantPosId : CRM.vars.payu['merchant-pos-id']
        };
    CRM.$.each( args, function( key, value ) {
        let field = CRM.$('<input type="hidden"></input>');
        field.attr("name", key);
        field.attr("value", value);
        form.append(field);
    });
    CRM.$(form).appendTo('body').submit();
};