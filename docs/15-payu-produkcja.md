# Utworzenie sklepu w środowisku produkcyjnym PayU

Przed utworzeniem sklepu **musi być** utworzona strona darowizny przy założeniach:

* procesor płatności tymczasowo skonfigurowany na sklep *CRM-TEST* z sandboxa
* strona darowizny zawiera link do regulaminu darowizn

Kroki są takie same jak podczas [tworzenia sklepu w sandboxie](05-payu-sandbox.md) z następującymi różnicami:

* logowanie do [panelu produkcyjnego](https://secure.payu.com/user/login) za pomocą konta klienta
* pole *Inny adres WWW* musi zawierać pełny adres url do strony darowizny regularnej
    * na podstawie tego adresu Support PayU weryfikuje sklep
* pole *Nazwa sklepu* : `CRM-PRODUKCJA`
* wykonanie zgłoszenia do Supportu PayU z prośbą o włączenie płatności cyklicznych podając id punktu dostępowego
    * wykonać dopiero po aktywacji sklepu
    * wykonać poprzez formularz dostępny z panelu PayU


Przykład:

![krok1](../images/05-produkcja-krok1.png)

![krok2](../images/06-produkcja-krok2.png)

![krok3](../images/07-produkcja-krok3.png)

![krok3 komunikat](../images/07a-produkcja-krok3-komunikat.png)

Sklep jeszcze niezweryfikowany widoczny jest na liście sklepów jako oznaczony na czerwono.

![niezweryfikowany sklep](../images/08-sklep-do-weryfikacji.png)

