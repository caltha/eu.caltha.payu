# Modyfikacja wyglądu i treści stron potwierdzeń

Zmiana domyślnych wyglądów realizowana jest poprzez:

* zmiany w skórce Drupal lub przygotowanie nowej
* przygotowanie stron podziękowań w rozszerzeniu CiviCRM

Strony do przygotowania:

* strony darowizny (poprzez skórkę Drupala)
* stron potwierdzeń (poprzez nowe szablony w rozszerzeniu CiviCRM)
    * regularne początkowa
    * regularne końcowa karta zwykła
    * regularne końcowa karta 3DS
    * jednorazowe udane
    * jednorazowe nieudane


## Nowe szablony w rozszerzeniu CiviCRM

Zakładając, że nowe rozszerzenie będzie miało nazwę *tweaks* to hooki podmieniające szablony wyglądają następująco:

```php
function tweaks_civicrm_alterTemplateFile($formName, &$form, $context, &$tplName) {
  if ($formName == 'CRM_Payu_Page_First') {
    $possibleTpl = 'CRM/Payu/Page/First.custom.tpl';
    $template = CRM_Core_Smarty::singleton();
    if ($template->template_exists($possibleTpl)) {
      $tplName = $possibleTpl;
    }
  }
  if ($formName == 'CRM_Payu_Page_Second') {
    $possibleTpl = 'CRM/Payu/Page/Second.custom.tpl';
    $template = CRM_Core_Smarty::singleton();
    if ($template->template_exists($possibleTpl)) {
      $tplName = $possibleTpl;
    }
  }
  if ($formName == 'CRM_Payu_Page_Summary_Recur') {
    $possibleTpl = 'CRM/Payu/Page/Summary/Recur.custom.tpl';
    $template = CRM_Core_Smarty::singleton();
    if ($template->template_exists($possibleTpl)) {
      $tplName = $possibleTpl;
    }
  }
  if ($formName == 'CRM_Payu_Page_Summary_Single') {
    $possibleTpl = 'CRM/Payu/Page/Summary/Single.custom.tpl';
    $template = CRM_Core_Smarty::singleton();
    if ($template->template_exists($possibleTpl)) {
      $tplName = $possibleTpl;
    }
  }
}
```
