# Utworzenie sklepu w sandboxie PayU

* Zaloguj się do panelu PayU
    * [wersja dla sandboxa](https://merch-prod.snd.payu.com/user/login)
* kliknij menu *Moje sklepy*
* kliknij przycisk *Dodaj sklep*
* wypełnij formularz *Dane sklepu*:
    * *Adres strony* : `inny adres`
    * *Inny adres WWW* : `https://crm.moja-organizacja.pl`
    * *Przedmio usług* : `Zbiórki pieniędzy`
    * *Nazwa sklepu* : `CRM-TEST`

![Dane sklepu](../images/01-dodanie-sklepu-krok1-dane-sklepu.png)

* wypełnij formularz *Punkt płatności* i kliknij przycisk *Dodaj sklep*
    * *Typ punktu płatności* : `REST API (Checkout)`
    * *Nazwa punktu płatności* : `ContributionPage`

![Punkt płatności](../images/02-dodanie-sklepu-krok2-punkt-platnosci.png)

* na podsumowaniu zostaną wyświetlone klucze konfiguracyjne utworzonego sklepu

![Klucze](../images/03-dodanie-sklepu-krok3-klucze.png)

* ponowne wyświetlenie kluczy konfiguracyjnych i dostępnych typów płatności klikając menu *Moje sklepy* > link w nagłówku sekcji *CRM-TEST* > zakładka *Punkty płatności* > link *ContributionPage*

![Dostępne typy płatności](../images/04-dostepne-typy-platnosci.png)
