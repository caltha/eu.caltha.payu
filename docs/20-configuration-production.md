# Konfiguracja CiviCRM pod środowisko produkcyjne

Na tym etapie są już wykonane

* instalacja i konfiguracja rozszerzenia
* skonfigurowany CiviCRM pod środowisko sandbox
    * utworzona strona transakcyjna


## 1. Przygotowanie regulaminu darowizn

* przygotuj wersję PDF regulaminu darowizn
* wrzuć plik na serwer np. do katalogu `public/sites/default/files/`
    * taki regulamin jest dostępny do pobrania poprzez link `https://crm.moja-organizacja.pl/sites/default/files/payu_regulamin_darowizn.pdf`.


## 2. Utworzenie stron darowizny jednorazowej i/lub darowizny regularnej

W zależności od planowanego zakresu użycia, tworzymy stronę transakcyjną do obsługi darowizn jednorazowych i ewentualnie osobną do darowizn regularnych.

Zalecamy utworzenie osobnych stron transakcyjnych ponieważ takie podejście pozwala przygotować różny wygląd w zależności od typu darowizny, jednorazowej lub regularnej.

Na stronach transakcyjnych należy umieścić link do regulaminu:

* na zakładce *Tytuł* w polu *Wiadomość w stopce* dodać link *Regulamin darowizn* wskazujący na przygotowany wcześniej plik PDF.


## 3. Aktualizacja procesora płatności

Zmieniamy wartości kluczy konfiguracyjnych w sekcji *Processor Details for Live Payments* wartościami dla sklepu `CRM-PRODUKCJA`.
