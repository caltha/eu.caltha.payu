# Struktura danych

## Typy interakcji

 | Nazwa techniczna (name)          | Etykietka                           | Pole dodatkowe    |
|----------------------------------|-------------------------------------|-------------------|
| `Deklaracja PayU - anulowanie`   | Deklaracja PayU - anulowanie        | Status pobrania   |
| `Deklaracja PayU - zamknięcie`   | Deklaracja PayU - zamknięcie        | Status pobrania   |
| `Deklaracja PayU - błąd`         | Deklaracja PayU - błąd              | Status pobrania   |

## Pola dodatkowe

| Etykieta pola    | Typ pola       | Wymagane? | Inne                                                                                           |
|------------------|----------------|-----------|------------------------------------------------------------------------------------------------|
| Status pobrania  | Select         | Nie       | [Źródło słownika](https://developers.payu.com/europe/pl/docs/card-payments/card-status-codes/) |
