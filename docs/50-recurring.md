# Recurring

## Jak ustawiane są statusy obiektów w zależności od statusu płatności kartowej?

| Zdarzenie     | Transakcja | Stała wpłata | Deklaracja   | Ważność karty |
|---------------|------------|--------------|--------------|---------------|
| new           | Pending    | Pending      | New          | New           |
| active        | Completed  | In progress  | Active       | Active        |
| cancel        | Cancelled  | (bez zmiany) | (bez zmiany) | (bez zmiany)  |
| fail          | Failed     | Failed       | Failed       | Failed        |
| close         | Failed     | Completed    | Closed       | Closed        |
| (nierozpozne) | Cancelled  | (bez zmiany) | (bez zmiany) | (bez zmiany)  |

**Legenda:**

* Zdarzenie - otrzymany kod `cardResponseCode` przypisany jest do jednej z grup *active*, *cancel*, *fail* lub *close*
    * *new* - na etapie zgłaszania deklaracji, endpoint nie otrzymuje kodu `cardResponseCode`
* Transakcja - comiesięczna transakcja
* Stała wpłata - transakcja recurring
* Deklaracja - interakcja *Deklaracja płatności cyklicznej*
* Ważność karty - interakcja *Ważność karty kredytowej*

**Uwaga!** Przypisanie do grup znajduje się w klasie `CRM_Payu_Model_CardStatus`.


## Przypisanie zdarzeń do statusów płatności kartowej

Lista kodów `cardResponseCode` pochodzi z [dokumentacji payu](https://developers.payu.com/europe/pl/docs/card-payments/card-status-codes/), *dostęp 2024-01-31*

**Uwaga!** Nie wszystkie statusy płatności kartowej zostały przypisane do jednej z grup ze względu na niejasne znaczenie.

| Zdarzenie | Kod | Opis                                                            |
|-----------|-----|-----------------------------------------------------------------|
| active    | 000 | 000 - OK                                                        |
| cancel    | S04 | S04 - Pickup card                                               |
| cancel    | S12 | S12 - Invalid transaction                                       |
| cancel    | S13 | S13 - Invalid amount                                            |
| cancel    | S51 | S51 - Insufficient funds                                        |
| cancel    | S61 | S61 - Exceeds approval amount                                   |
| cancel    | SP1 | SP1 - Over daily limit (try again later)                        |
| fail      | INV | INV - Invalid Token                                             |
| fail      | S05 | S05 - Do not honor                                              |
| fail      | S43 | S43 - Pickup card (stolen card)                                 |
| fail      | S57 | S57 - Card disabled for e-commerce or cross-border transactions |
| fail      | S62 | S62 - Restricted card / Country exclusion table                 |
| fail      | S93 | S93 - Card disabled for e-commerce transactions                 |
| fail      | S99 | S99 - authorization error – default                             |
| fail      | SN0 | SN0 - Unable to authorize / Force STIP                          |
| fail      | ST3 | ST3 - Card not supported                                        |
| fail      | ST8 | ST8 - Invalid account                                           |
| fail      | SSD | SSD - Soft decline (strong authentication required)             |
| fail      | 114 | 114 - 3ds authentication error                                  |
| fail      | 115 | 115 - 3ds authentication error                                  |
| fail      | 116 | 116 - 3ds authentication error                                  |
| fail      | 117 | 117 - 3ds authentication error                                  |
| fail      | 120 | 120 - 3ds processing error                                      |
| fail      | 123 | 123 - Missing cryptogram for Network Token\'s authorization.    |
| fail      | 130 | 130 - Non-compliant prepaid card                                |
| fail      | 132 | 132 - Issuer will never approve                                 |
| fail      | SIN | SIN - No such issuer                                            |
| fail      | SAC | SAC - Account closed (do not try again)                         |
| fail      | SPF | SPF - Possible fraud (do not try again)                         |
| close     | S54 | S54 - Expired card                                              |
| ?         | S01 | S01 - Refer to card issuer                                      |
| ?         | S30 | S30 - Message format error                                      |
| ?         | S65 | S65 - Exceeds withdrawal frequency limit                        |
| ?         | S90 | S90 - Destination not available                                 |
| ?         | SP9 | SP9 - Enter lesser amount                                       |
| ?         | 222 | 222 - Transaction not received by merchant                      |
| ?         | 001 | 001 – techniczny                                                |
| ?         | 002 | 002 – techniczny                                                |
| ?         | 003 | 003 – techniczny                                                |
| ?         | 004 | 004 – techniczny                                                |
| ?         | 005 | 005 – techniczny                                                |
| ?         | 110 | 110 – antyfrauding                                              |
| ?         | 111 | 111 – antyfrauding                                              |
| ?         | 112 | 112 – antyfrauding                                              |
| ?         | 113 | 113 – antyfrauding                                              |
| ?         | 121 | 121 – antyfrauding                                              |
| ?         | 122 | 122 – antyfrauding                                              |
| ?         | 221 | 221 – antyfrauding                                              |
| ?         | 223 | 223 – antyfrauding                                              |

## Próba pobrania deklaracji cyklicznej

* Jeśli w ustawieniach rozszerzenia włączysz opcję *Extend history* to rozszerzenie będzie zapisywać wszystkie próby pobrania płatności jako interakcję typu `Próba pobrania deklaracji cyklicznej`.
* Interakcja ta ma status wynikający ze statusu płatności kartowej
    * tytuł interakcji zawiera kod i opis odpowiedzi
