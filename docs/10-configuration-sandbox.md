# Konfiguracja CiviCRM pod środowisko sandbox

## 1. Utworzenie procesora płatności

* wejdź na stronę *Administruj > CiviContribute > Procesory płatności*
* kliknij przycisk *Dodaj procesor płatności*
* wypełnij formularz:
    * *Payment Processor Type* : `PayU`
    * *System płatności* : `PayU`
    * *Konto księgowe* : `Konto procesora płatności`
    * *Metoda płatności* : `PayU`
    * sekcje *Processor Details for Live Payments* i *Processor Details for Test Payments* wypełniamy tymi samymi danych punktu płatności z sandboxa
* po kliknięciu przycisku *Zapisz* zwróć uwagę na identyfikatory
    * kolumna *identyfikator* to id procesora dla wersji Live
    * kolumna *Test ID* to id procesora dla wersji Test

Przykład:

![Przykład konfiguracji procesora płatności](../images/procesor-platnosci.png)


## 2. Włączenie uprawnień drupalowych

* wejdź na stronę Drupal *Ludzie* > zakładka *Uprawnienia*
* włącz uprawnienie dla roli *anonymous users* / *użytkownik anonimowy*
    * pl: *CiviContribute: dokonaj wpłat online*
    * en: *CiviContribute: make online contributions*
* włącz uprawnienie dla roli *anonymous users* / *użytkownik anonimowy*
    * pl: *CiviCRM: tworzenie profili*
    * en: *CiviCRM: profile create*

## 3. Utworzenie strony darowizny

> W tym kroku należy zdecydować w jakiej formie bedą przyjmowane darowizy ( **jednorazowe**, **jednorazowe + regularne** ).
> Płatności regularne **nie są domyślnie uruchomione**, aby z nich korzystać należy skontaktować się z obsługą PayU aby poznać dodatkowe warunki/opłaty, od strony wtyczki nie wymaga to dodatkowej konfiguracji.

* wejdź na stronę *Transakcje > Zarządzaj stronami transakcyjnymi*
* kliknij przycisk *Dodaj stronę transakcyjną*
* wypełnij formularz i kliknij przycisk *Dalej*:
    * *Tytuł* : `Darowizna`
    * jeśli strona ma obsługiwać tylko płatności jednorazowe:
        * *Financial Type ID* : `PayU Jednorazowy`
    * jeśli strona ma obsługiwać płatności jednorazowe oraz cykliczne:
        * *Financial Type ID* : `PayU Cykliczny`
        * włącz opcję *Stałe wpłaty* oraz pozostaw zaznaczony miesiąc
    * *Używać strony potwierdzenia?* : odznaczyć
* na zakładce *Wpłaty* wypełnij formularz:
    * *System płatności* : zaznacz `PayU`
    * *Pozwól na dowolną kwotę* : zaznacz opcję, podaj kwoty min/max
    * wypełnij kilka pozycji z przykładowymi kwotami, 10 zł, 20 zł czy 50 zł
* w zakładce *Profile* wypełnij formularz i kliknij przycisk *Zapisz i zakończ*:
    * *Załącz profil CiviCRMowy (góra strony)* : `Dane osobowe` czyli profil zawierający imię, nazwisko i email


## 4. Dodanie zadań zaplanowanych obsługujących darowizny regularne

> Punkt dotyczy wyłącznie opcji *Financial Type ID* ustawionej na `PayU Cykliczny`

* wejdź na stronę *Administruj > Ustawienia systemu > Zadania zaplanowane*
* kliknij przycisk *Dodaj nowe zaplanowane zadanie*
* wypełnij formularz i kliknij przycisk *Zapisz*
    * *Nazwa* : `Call PayU for recurring monthly donations (live, production processor)`
    * *API call* : `Payu.order`
    * *Parametry polecenia* : `payment_processor_id=1` jeśli pole *Identyfikator* procesora równe jest `1`
* kliknij jeszcze raz przycisk *Dodaj nowe zaplanowane zadanie*
* wypełnij formularz i kliknij przycisk *Zapisz*
    * *Nazwa* : `Call PayU for recurring monthly donations (testing, sandbox processor)`
    * *API call* : `Payu.order`
    * *Parametry polecenia* : `payment_processor_id=2` jeśli pole *Test ID* procesora równe jest `2`

Przykład:

![przykład nowego zadania](../images/nowe-zadanie.png)


## 5. Zapewnienie dostępności strony notyfikacji z zewnątrz

Wiążący status transakcji ustawiany jest przez notyfikację otrzymaną z payu na jeden z adresów url w CiviCRM.

Jeśli serwer jest w jakiś sposób zabezpieczony (np. .htaccess) to należy utworzyć wyjątek na poniższe adresy:

* */civicrm/payu/notify-recur?pp=X* gdzie `X` to id procesora płatności *test* i/lub *live*
* */civicrm/payu/notify-single?pp=X* gdzie `X` to id procesora płatności *test* i/lub *live*
