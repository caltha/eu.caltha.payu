# Instalacja i konfiguracja rozszerzenia

## Wymagania

* PHP v7.3+
* Composer 2
* CiviCRM 5.43

## Instalacja

```shell
cd public/sites/default/files/civicrm/ext/
wget https://bitbucket.org/caltha/eu.caltha.payu/get/0.3.1.tar.gz
tar -xzf 0.3.1.tar.gz
mv caltha-eu.caltha.payu-*/ eu.caltha.payu/

cd eu.caltha.payu/packages/
composer install
```

> Przed włączeniem wtyczki lub w wypadku pojawienia się błędów podczas instalacji/dalszych etapach konfiguracji wtyczki, np. strony płatności zalecane jest wyczyszczenie cache: `cv flush` lub `drush cache-clear civicrm`

Wtyczkę można włączyć w panelu administracyjnym *Administruj > Ustawienia systemu > Rozszerzenia* lub bezpośrenio z konsoli z pomocą komendy `cv`:

```shell
cv ext:enable eu.caltha.payu
```

## Konfiguracja rozszerzenia

Konfiguracja rozszerzenia dostępna jest na stronie *Administruj > Pulpit administracyjny > Caltha PayU settings*

* *Interval in days between attempts of debiting* : domyślnie `1` dzień
* *Extend history* : domyślnie wyłączone, **włączyć**

Po instalacji należy poprawić konfigurację metody płatności PayU:

* otwórz stronę *Administruj > CiviContribute > Metody płatności*
* w wierszu metody *PayU* kliknij link *Edytuj*
* w polu *Konto księgowe* wybierz pozycję *Konto procesora płatności*
* kliknij przycisk *Zapisz*

![Ustawienie konta księgowego metodzie PayU](../images/metoda-platnosci-konto-ksiegowe.png)
