<?php

require_once 'payu.civix.php';
use CRM_Payu_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_config
 */
function payu_civicrm_config(&$config) {
  _payu_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_install
 */
function payu_civicrm_install() {
  _payu_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_enable
 */
function payu_civicrm_enable() {
  _payu_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_check().
 */
function payu_civicrm_check(&$messages) {
  if (!CRM_Extension_System::singleton()->getMapper()->isActiveModule('toolbox')) {
    $messages[] = new CRM_Utils_Check_Message(
      'payu_toolbox',
      E::ts('Extension %1 requires installation of eu.caltha.toolbox', [1 => E::LONG_NAME]),
      E::ts('Payu Toolbox'),
      \Psr\Log\LogLevel::CRITICAL
    );
  }
}

/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_navigationMenu
 */
function payu_civicrm_navigationMenu(&$menu): void {
  _payu_civix_insert_navigation_menu($menu, 'Administer/CiviContribute', [
    'label' => E::ts('Caltha PayU Settings'),
    'name' => 'payu_settings',
    'url' => 'civicrm/admin/setting/payu',
    'permission' => 'administer CiviCRM',
    'operator' => 'OR',
    'separator' => '0',
  ]);
  _payu_civix_navigationMenu($menu);
}
/**
 * Implements hook_civicrm_navigationMenu().
 *
 * @link http://wiki.civicrm.org/confluence/display/CRMDOC/hook_civicrm_navigationMenu
 *
function payu_civicrm_navigationMenu(&$menu) {
  _payu_civix_insert_navigation_menu($menu, NULL, array(
    'label' => E::ts('The Page'),
    'name' => 'the_page',
    'url' => 'civicrm/the-page',
    'permission' => 'access CiviReport,access CiviContribute',
    'operator' => 'OR',
    'separator' => 0,
  ));
  _payu_civix_navigationMenu($menu);
} // */

// /**
//  * Implements hook_civicrm_entityTypes().
//  *
//  * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
//  */
// function payu_civicrm_entityTypes(&$entityTypes) {
//   _payu_civix_civicrm_entityTypes($entityTypes);
// }


function payu_civicrm_permission(&$permissions) {
	$permissions += [
		'change amount payu' => [
			'label'			=>	E::ts('PayU: Change Amount'),
			'description'	=>	E::ts('Change recurring contribution amount')
		],
	];
}

function payu_civicrm_links(string $op, ?string $objectName, $objectID, array &$links, ?int &$mask, array &$values): void {
	if(CRM_Core_Permission::check('change amount payu')){
		if($op == 'contribution.selector.recurring'){
		  $contributeID = $values['crid'];

      $contributionRecurs = \Civi\Api4\ContributionRecur::get(FALSE)
        ->addSelect('payment_processor_id.payment_processor_type_id:name','contribution_status_id:name')
        ->addWhere('id', '=', $contributeID)
        ->execute();

      if($contributionRecurs[0]['payment_processor_id.payment_processor_type_id:name'] == 'Payu'){
        $possibleStatuses = array('Pending','In Progress');
        foreach($possibleStatuses as $status){
          if($status == $contributionRecurs[0]['contribution_status_id:name']){
            $show = true;
          }
        }
        if($show == true) {
          $links['zmianaKwoty'] = [
            'name' => E::ts('Zmiana kwoty'),
            'title' => E::ts('Zmiana kwoty'),
            'url' => 'civicrm/payu/change-amount',
            'qs' => 'reset=1&crid=%%crid%%&cid=%%cid%%&context=contribution',
            'weight' => 30,
          ];
        }
      }
		}
	}
}
