<?php
require_once __DIR__ . '/../../../packages/vendor/autoload.php';
use CRM_Payu_ExtensionUtil as E;
function _civicrm_api3_payu_order_spec(&$spec) {
  $spec['payment_processor_id'] = [
    'name' => 'payment_processor_id',
    'title' => 'Payment Processor Id',
    'description' => 'Payment Processor Id',
    'type' => CRM_Utils_Type::T_INT,
    'api.required' => 1,
    'api.default' => 0,
  ];
}
function is_cli()
{
  if ( defined('STDIN') )
  {
    return true;
  }

  if ( php_sapi_name() === 'cli' )
  {
    return true;
  }

  if ( array_key_exists('SHELL', $_ENV) ) {
    return true;
  }

  if ( empty($_SERVER['REMOTE_ADDR']) and !isset($_SERVER['HTTP_USER_AGENT']) and count($_SERVER['argv']) > 0)
  {
    return true;
  }

  if ( !array_key_exists('REQUEST_METHOD', $_SERVER) )
  {
    return true;
  }

  return false;
}
function dbacktrace() {
  $keys = array('file','line');
  $target = array();
  foreach(debug_backtrace() as $key => $s) {
    /*if ($key == 0) {
      continue;
    }*/

    foreach($s as $inner_key => $inner_s) {
      if (in_array($inner_key,$keys))
        $target[$key][$inner_key] = $inner_s;
    }
  }

  return $target;
}
/**
 * Call PayU for recurring monthly donations
 *
 * @param $params
 *
 * @return array
 * @throws \CiviCRM_API3_Exception
 * @throws \OpenPayU_Exception_Configuration
 * @throws \OpenPayU_Exception
 * @throws \Exception
 */
function civicrm_api3_payu_order(&$params) {
  $lock = new CRM_Toolbox_LockManager(__FILE__, E::LONG_NAME);
  if ($lock->isRunning()) {
    return $lock->createError();
  }
  
  \Civi::log()->info(is_cli() ? 'CRON BY CLI' : 'CRON BY OTHER');
  \Civi::log()->info(
    var_export(dbacktrace(), true)
  );
  \Civi::log()->info(
    var_export($_SERVER, true)
  );
  \Civi::log()->info(
    var_export($_REQUEST, true)
  );

  $start = microtime(TRUE);
  $paymentProcessorId = (int) $params['payment_processor_id'];
  $processor = CRM_Financial_BAO_PaymentProcessor::findById($paymentProcessorId);
  CRM_Payu_Config::initByObject($processor);
  $next = new CRM_Payu_Logic_Order_Next();
  $values = $next->callFirst($paymentProcessorId);
  $values2 = $next->callSecond($paymentProcessorId);
  $values3 = $next->callThird($paymentProcessorId);
  $values = array_merge($values, $values2, $values3);
  $extraReturnValues = array(
    'time' => microtime(TRUE) - $start,
  );
  $lock->release();
  return civicrm_api3_create_success($values, $params, 'Payu', 'order', $blank, $extraReturnValues);
}
