# PayU payment processor

Rozszerzenie udostępnia w CiviCRM procesor płatności **PayU**.

Funkcjonalności:

* płatności jednorazowe
* płatności regularne
* możliwość modyfikowania wyglądu stron potwierdzeń

Po wdrożeniu rozszerzenia w CiviCRM dostępne będą następujące typy obiektów:

* typy finansowe transakcji: `PayU Jednorazowy`, `PayU Cykliczny`
* metoda płatności: `PayU`
* typ procesora płatności: `PayU`
* typy aktywności: `Deklaracja płatności cyklicznej`, `Ważność karty kredytowej`, `Próba pobrania deklaracji cyklicznej`
* statusy aktywności: `New`, `Active`, `Cancelled`, `Failed`, `Closed`
* zestaw pól dodatkowych `PayU` przypisany do aktywności typu `Deklaracja płatności cyklicznej` i `Ważność karty kredytowej`
* zestaw pól dodatkowych `PayU Contribution` przypisany do transakcji typu `Payu Cykliczny`


## Wdrożenie sklepu testowego

Najpierw należy wdrożyć obsługę sklepu testowego ze środowiska sandbox PayU a w kolejnym kroku wdrożyć wersję produkcyjną

* [zainstaluj rozszerzenie](docs/00-install.md) i skonfiguruj
* [utwórz sklep testowy w środowisku sandbox](docs/05-payu-sandbox.md)
* [skonfiguruj CiviCRM pod środowisko sandbox](docs/10-configuration-sandbox.md) w tym tworzenie pierwszej strony transakcyjnej
* opcjonalnie [modyfikacja wyglądu i treści stron potwierdzeń](docs/30-custom-templates.md)


## Wdrożenie sklepu produkcyjnego

Wykonać po wdrożeniu sklepu testowego.

* [utwórz docelowy sklep w środowisku produkcyjnym](docs/15-payu-produkcja.md)
* [skonfiguruj CiviCRM pod środowisko produkcyjne](docs/20-configuration-production.md)
