{* HEADER *}
<h3>Aktualne dane</h3>
<div class="crm-submit-buttons"></div>

{* FIELD EXAMPLE: OPTION 1 	(AUTOMATIC LAYOUT) *}
<table class="crm-info-panel">
	<tbody>
		<tr>
			<td class="label">Kwota:</td>
			<td>{$contributionRecurData.amount} {$contributionRecurData.currency}</td>
		<tr>
		<tr>
			<td class="label">Data rozpoczęcia:</td>
			<td>{$contributionRecurData.start_date}</td>
		</tr>
	</tbody>
</table>

{foreach from=$elementNames item=elementName}
  <div class="crm-section">
    <div class="label">{$form.$elementName.label}</div>
    <div class="content">{$form.$elementName.html}</div>
    <div class="clear"></div>
  </div>
{/foreach}

{* FIELD EXAMPLE: OPTION 2 (MANUAL LAYOUT)



{* FOOTER *}
<div class="crm-submit-buttons">
{include file="CRM/common/formButtons.tpl" location="bottom"}
</div>
