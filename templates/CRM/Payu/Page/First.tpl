{crmScope extensionKey='eu.caltha.payu'}
  <h1>{ts}Confirm recurring payment{/ts}</h1>

  <p>{ts 1=$totalAmount 2=$currencyCode}Płatność cykliczna na kwotę %1 %2 (pierwsza płatność){/ts}</p>
{/crmScope}
