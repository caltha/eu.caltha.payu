<h1>{ts}Summary{/ts}</h1>

{if $verified}
  {if $orderStatus eq 'COMPLETED'}
    <p>Dziękujemy za wpłatę na kwotę {$totalAmount} {$currency}! :-D</p>

  {elseif $orderStatus eq 'PENDING' or $orderStatus eq 'WAITING_FOR_CONFIRMATION'}
    <p>Twoja wpłata na kwotę {$totalAmount} {$currency} jest w trakcie realizacji, dziękujemy!</p>

  {elseif $orderStatus eq 'CANCELED'}
    <p>Niestety, wpłata na kwotę {$totalAmount} {$currency} zakończyła się niepowodzeniem :(</p>

  {elseif $orderStatus eq 'REJECTED'}
    <p>Niestety Twoja wpłata została odrzucona.</p>

  {/if}

{else}
  <p>
    Bardzo nam przykro, ale nie znaleźliśmy takiej transakcji :(
  </p>
{/if}
