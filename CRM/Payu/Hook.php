<?php

class CRM_Payu_Hook {

  static $_nullObject = NULL;

  static function alterSummaryPage(&$page, $pageClass, $response) {
    return CRM_Utils_Hook::singleton()
      ->invoke(['page', 'pageClass', 'response'], $page, $pageClass, $response,
        self::$_nullObject, self::$_nullObject, self::$_nullObject,
        'payu_alterSummaryPage'
      );
  }
}
