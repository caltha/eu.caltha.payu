<?php

require_once __DIR__ . '/../../packages/vendor/autoload.php';

class CRM_Payu_Config {

  public static $urlSite = '';
  public static $urlApi = '';

  /**
   * Initialize PayU Payment Processor by id
   *
   * @param int $paymentProcessorId
   *
   * @throws \OpenPayU_Exception_Configuration
   */
  public static function initById($paymentProcessorId) {
    $pp = new CRM_Financial_BAO_PaymentProcessor();
    $pp->id = $paymentProcessorId;
    $pp->find();
    $pp->fetch();
    CRM_Payu_Config::initByObject($pp);
  }

  /**
   * @param $paymentProcessor
   *
   * @throws \OpenPayU_Exception_Configuration
   */
  public static function initByObject($paymentProcessor) {
    $proc = [
      'user_name' => $paymentProcessor->user_name,
      'password' => $paymentProcessor->password,
      'signature' => $paymentProcessor->signature,
      'subject' => $paymentProcessor->subject,
      'url_site' => $paymentProcessor->url_site,
      'url_api' => $paymentProcessor->url_api,
      'is_test' => $paymentProcessor->is_test,
    ];
    self::init($proc);
  }

  /**
   * @param $params
   *
   * @throws \OpenPayU_Exception_Configuration
   */
  public static function init($params) {
    $env = self::setEnviroment($params);
    OpenPayU_Configuration::setEnvironment($env);
    OpenPayU_Configuration::setMerchantPosId($params['user_name']);
    OpenPayU_Configuration::setSignatureKey($params['password']);
    OpenPayU_Configuration::setOauthClientId($params['signature']);
    OpenPayU_Configuration::setOauthClientSecret($params['subject']);
    self::$urlSite = $params['url_site'];
    self::$urlApi = $params['url_api'];
  }

  private static function setEnviroment($params) {
    if (array_key_exists('is_test', $params) && $params['is_test']) {
      return 'sandbox';
    }
    return 'secure';
  }

}
