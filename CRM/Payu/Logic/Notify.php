<?php

class CRM_Payu_Logic_Notify {

  /**
   * Płatność jest w trakcie rozliczenia.
   */
  const STATUS_PENDING = 'PENDING';

  /**
   * system PayU oczekuje na akcje ze strony sprzedawcy w celu wykonania płatności.
   * Ten status występuje w przypadku gdy auto-odbiór na POSie sprzedawcy jest wyłączony.
   */
  const STATUS_WAITING_FOR_CONFIRMATION = 'WAITING_FOR_CONFIRMATION';

  /**
   * Płatność została odrzucona, ale płacący został obciążony
   * (nastąpił przepływ środków między płacącym a Payu).
   * Taką płatność można odebrać (środki zostaną przekazane do sklepu)
   * lub anulować (środki zostaną zwrocone płacącemu).
   */
  const STATUS_REJECTED = 'REJECTED';

  /**
   * Płatność została zaakceptowana w całości. Środki są dostępne do wypłaty.
   */
  const STATUS_COMPLETED = 'COMPLETED';

  /**
   * Płatność została anulowana. Płacący nie został obciążony
   * (nie nastąpił przepływ środków między płacącym a Payu).
   */
  const STATUS_CANCELED = 'CANCELED';

  public $sender = '';
  public $signature = '';
  public $algorithm = '';
  public $content = '';

  /**
   * Parse header from PayU and return array with 0:signature and 1:algorithm
   * @param string $rawHeaderSignature
   *
   * @return array
   */
  public function parseSignature($rawHeaderSignature) {
    $tab = explode(';', $rawHeaderSignature);
    foreach ($tab as $item) {
      $v = explode('=', $item);
      $key = $v[0];
      $this->$key = $v[1];
    }

    return [$this->signature, $this->algorithm];
  }

}
