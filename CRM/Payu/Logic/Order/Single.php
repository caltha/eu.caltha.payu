<?php

/**
 * Class handles PayU Order (not CiviCRM order/contribution)
 */
class CRM_Payu_Logic_Order_Single extends CRM_Payu_Logic_Order {

  /**
   * Find all needed data for preparing PayU Order.
   *
   * @param int $contributionId
   *
   * @return \CRM_Core_DAO|object
   */
  public function find($contributionId) {
    $query = "SELECT ct.id, ct.source, ct.currency, ct.total_amount, ct.invoice_id,
                c.first_name, c.last_name, c.preferred_language, e.email, ct.is_test
              FROM civicrm_contribution ct
                JOIN civicrm_contact c ON c.id = ct.contact_id
                JOIN civicrm_email e ON e.contact_id = ct.contact_id AND e.is_primary = 1
              WHERE ct.id = %1";
    $params = [
      1 => [$contributionId, 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $params);
    $dao->fetch();
    $result = $dao;
    $dao->free();

    return $result;
  }

  /**
   * Prepare PayU Order params for first payment.
   *
   * @param int $paymentProcessorId
   * @param int $merchantPosId
   * @param int $contributionId
   * @param object $dao
   *
   * @return array
   */
  public function prepare($paymentProcessorId, $merchantPosId, $contributionId, $dao) {
    $extOrderId = CRM_Payu_Logic_Contribution::encodeIdSingle($contributionId, 1);
    return parent::prepareOrder($paymentProcessorId, $merchantPosId, $extOrderId, $dao);
  }

}
