<?php
require_once __DIR__ . '/../../../../packages/vendor/autoload.php';

/**
 * Class handles PayU Order (not CiviCRM order/contribution)
 */
class CRM_Payu_Logic_Order_Next extends CRM_Payu_Logic_Order {

  private $pending = 2;
  private $inProgress;

  public function __construct() {
    $this->inProgress = (int) CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'In Progress');
  }

  /**
   * Find all needed data for preparing PayU Order.
   *
   * @param $contributionRecurId
   *
   * @return \CRM_Core_DAO|object
   */
  public function find($contributionRecurId) {
    $query = "SELECT cr.id, cr.contact_id, cr.amount total_amount, cr.currency, (
                  SELECT source FROM civicrm_contribution WHERE contribution_recur_id = cr.id LIMIT 1
                ) AS source, (
                  SELECT contribution_page_id FROM civicrm_contribution WHERE contribution_recur_id = cr.id LIMIT 1
                ) AS contribution_page_id, cr.financial_type_id, cr.payment_instrument_id,
                cr.campaign_id, cr.is_test, substr(cr.trxn_id FROM 1 FOR position('.' IN trxn_id) - 1) token,
                c.first_name, c.last_name, c.preferred_language, e.email,
                cr.payment_processor_id,
                pp.name processor_name,  pp.user_name, pp.password, pp.signature, pp.subject, pp.url_site, pp.url_api
              FROM
                civicrm_contribution_recur cr
                JOIN civicrm_payment_processor pp ON pp.id = cr.payment_processor_id
                JOIN civicrm_contact c ON c.id = cr.contact_id
                JOIN civicrm_email e ON e.contact_id = cr.contact_id AND e.is_primary = 1
              WHERE cr.id = %1";
    $params = [
      1 => [$contributionRecurId, 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $params);
    if(!$dao->fetch()) {
      throw new Exception('Could not find contact with contributionRecurId: '. $contributionRecurId);
    }
    $result = $dao;
    $dao->free();

    return $result;
  }

  /**
   * Create next contribution using Order.create api action.
   *
   * @param object $recur
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function create($recur) {
    $params = [
      'financial_type_id' => $recur->financial_type_id,
      'total_amount' => $recur->total_amount,
      'contact_id' => $recur->contact_id,
      'contribution_page_id' => $recur->contribution_page_id,
      'receive_date' => date('YmdHis'),
      'payment_instrument_id' => $recur->payment_instrument_id,
      'source' => $recur->source,
      'is_test' => $recur->is_test,
      'contribution_status_id' => $this->pending,
      'campaign_id' => $recur->campaign_id,
      'currency' => $recur->currency,
      'contribution_recur_id' => $recur->id,
      'line_items' => [
        [
          'line_item' => [
            [
              'qty' => 1,
              'unit_price' => $recur->total_amount,
              'line_total' => $recur->total_amount,
              'price_field_id' => 1,
            ],
          ],
        ],
      ],
    ];
    $result = civicrm_api3('Order', 'create', $params);

    return $result;
  }

  /**
   * Prepare PayU Order for first payment.
   *
   * @param int $paymentProcessorId
   * @param int $merchantPosId
   * @param int $contributionId
   * @param object $dao
   * @param string $token
   * @param int $attempt
   *
   * @return array
   */
  public function prepare($paymentProcessorId, $merchantPosId, $contributionId, $dao, $token, $attempt = 1) {
    $recurring = self::RECURRING_STANDARD;
    $extOrderId = CRM_Payu_Logic_Contribution::encodeIdRecur($contributionId, $attempt);
    return parent::prepareOrder($paymentProcessorId, $merchantPosId, $extOrderId, $dao, $token, $recurring);
  }

  /**
   * @param $paymentProcessorId
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function callFirst($paymentProcessorId) {
    $query = "SELECT
                cr.id, cr.trxn_id
              FROM civicrm_contribution_recur cr
                LEFT JOIN civicrm_contribution ct ON ct.contribution_recur_id = cr.id
                    AND DATE_FORMAT(ct.receive_date, '%Y-%m') = DATE_FORMAT(CURRENT_DATE, '%Y-%m')
              WHERE cr.payment_instrument_id = %1
                AND cr.contribution_status_id = %2
                AND cr.payment_processor_id = %3
                AND cr.frequency_unit = 'month'
                AND cr.frequency_interval = 1
                AND cr.cycle_day <= DATE_FORMAT(CURRENT_DATE, '%d')
                AND ct.id IS NULL";
    $queryParams = [
      1 => [CRM_Payu_Model_PaymentMethod::payu(), 'Integer'],
      2 => [$this->inProgress, 'Integer'],
      3 => [$paymentProcessorId, 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $queryParams);
    $values = [];
    while ($dao->fetch()) {
      try {
        $recur = $this->find($dao->id);
        $contribution = $this->create($recur);
        $status = $this->request($dao->id, 1, $contribution['id']);
        $values[$dao->id] = [
          'status' => $status,
          'contribution_id' => $contribution['id'],
          'trxn_id' => $dao->trxn_id,
        ];
      }
      catch (Exception $exception) {
        $values[$dao->id] = [
          'status' => $exception->getMessage(),
          'trxn_id' => $dao->trxn_id,
        ];
      }
    }
    $dao->free();

    return $values;
  }

  /**
   * @param $paymentProcessorId
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function callSecond($paymentProcessorId) {
    $query = "SELECT ct.id, cr.trxn_id, ct.contribution_recur_id
              FROM civicrm_value_payu_contribution vpc
                JOIN civicrm_contribution ct ON ct.id = vpc.entity_id
                JOIN civicrm_contribution_recur cr ON cr.id = ct.contribution_recur_id
              WHERE status_1_proby_pobrania <> %4
                AND status_2_proby_pobrania IS NULL
                AND status_3_proby_pobrania IS NULL
                AND DATE_FORMAT(data_1_proby_pobrania, '%Y-%m-%d') = DATE_ADD(CURRENT_DATE, INTERVAL -%5 DAY)
                AND cr.payment_instrument_id = %1
                AND cr.contribution_status_id = %2
                AND cr.payment_processor_id = %3";
    $queryParams = [
      1 => [CRM_Payu_Model_PaymentMethod::payu(), 'Integer'],
      2 => [$this->inProgress, 'Integer'],
      3 => [$paymentProcessorId, 'Integer'],
      4 => [CRM_Payu_Model_CardStatus::OK_000, 'String'],
      5 => [Civi::settings()->get('payu_interval_days'), 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $queryParams);
    $values = [];
    while ($dao->fetch()) {
      $status = $this->request($dao->contribution_recur_id, 2, $dao->id);
      $values[$dao->contribution_recur_id] = [
        'status' => $status,
        'contribution_id' => $dao->id,
        'trxn_id' => $dao->trxn_id,
      ];
    }
    $dao->free();

    return $values;
  }

  /**
   * @param $paymentProcessorId
   *
   * @return array
   * @throws \CiviCRM_API3_Exception
   */
  public function callThird($paymentProcessorId) {
    $query = "SELECT ct.id, cr.trxn_id, ct.contribution_recur_id
              FROM civicrm_value_payu_contribution vpc
                JOIN civicrm_contribution ct ON ct.id = vpc.entity_id
                JOIN civicrm_contribution_recur cr ON cr.id = ct.contribution_recur_id
              WHERE status_1_proby_pobrania <> %4
                AND status_2_proby_pobrania <> %4
                AND status_3_proby_pobrania IS NULL
                AND DATE_FORMAT(data_2_proby_pobrania, '%Y-%m-%d') = DATE_ADD(CURRENT_DATE, INTERVAL -%5 DAY)
                AND cr.payment_instrument_id = %1
                AND cr.contribution_status_id = %2
                AND cr.payment_processor_id = %3";
    $queryParams = [
      1 => [CRM_Payu_Model_PaymentMethod::payu(), 'Integer'],
      2 => [$this->inProgress, 'Integer'],
      3 => [$paymentProcessorId, 'Integer'],
      4 => [CRM_Payu_Model_CardStatus::OK_000, 'String'],
      5 => [Civi::settings()->get('payu_interval_days'), 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $queryParams);
    $values = [];
    while ($dao->fetch()) {
      $status = $this->request($dao->contribution_recur_id, 3, $dao->id);
      $values[$dao->contribution_recur_id] = [
        'status' => $status,
        'contribution_id' => $dao->id,
        'trxn_id' => $dao->trxn_id,
      ];
    }
    $dao->free();

    return $values;
  }

  /**
   * @param int $recurId
   * @param int $attempt
   * @param int $contributionId
   *
   * @return string
   * @throws \CiviCRM_API3_Exception
   */
  private function request($recurId, $attempt = 1, $contributionId = 0) {
    $recur = $this->find($recurId);
    $orderParams = $this->prepare($recur->payment_processor_id, $recur->user_name, $contributionId, $recur, $recur->token, $attempt);
    try {
      $order = OpenPayU_Order::create($orderParams);
      $response = $order->getResponse();
      $this->saveOrderId($contributionId, $response->orderId);
      return $order->getStatus();
    }
    catch (OpenPayU_Exception $exception) {
      $message = $exception->getMessage();
      if ($this->isErrorValueInvalid($message)) {
        $this->processInvalidToken($contributionId, $message);
      }
      return $message . " (contribution_id:" . $contributionId . ")";

    }
    catch (Exception $exception) {
      return $exception->getMessage();
    }
  }

  /**
   * @param string $message
   *
   * @return false|int
   */
  private function isErrorValueInvalid($message) {
    $pattern = '/' . CRM_Payu_Model_Exception::ERROR_VALUE_INVALID . '/';

    return preg_match($pattern, $message);
  }

  /**
   * @param $contributionId
   * @param $message
   *
   * @throws \CiviCRM_API3_Exception
   */
  private function processInvalidToken($contributionId, $message) {
    $single = new CRM_Payu_Logic_Contribution_Single();
    $contribution = $single->find($contributionId);
    $recur = new CRM_Payu_Logic_Contribution_Recurring();
    $recur->failed($contribution->contribution_recur_id);
    $single->cancel($contributionId);
    $single->addNote($contributionId, $message);
    CRM_Payu_Logic_Activity_DeklaracjaPlatnosci::setFailed($contribution->contribution_recur_id);
    CRM_Payu_Logic_Activity_WaznoscKarty::setFailed($contribution->contribution_recur_id);
    $card = new CRM_Payu_Logic_Card();
    $card->saveCardStatus($contributionId, CRM_Payu_Model_CardStatus::INVALID_TOKEN, 1);
    $card->saveCardStatus($contributionId, CRM_Payu_Model_CardStatus::INVALID_TOKEN, 2);
    $card->saveCardStatus($contributionId, CRM_Payu_Model_CardStatus::INVALID_TOKEN, 3);
    $recurring = $recur->find($contribution->contribution_recur_id);
    CRM_Payu_Logic_Activity_StatusDeklaracji::failed(CRM_Payu_Model_CardStatus::INVALID_TOKEN, $recurring);
  }

}
