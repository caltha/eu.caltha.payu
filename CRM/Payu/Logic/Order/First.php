<?php

/**
 * Class handles PayU Order (not CiviCRM order/contribution)
 */
class CRM_Payu_Logic_Order_First extends CRM_Payu_Logic_Order {

  /**
   * Find all needed data for preparing PayU Order.
   *
   * @param int $contributionId
   *
   * @return \CRM_Core_DAO|object
   */
  public function find($contributionId) {
    $query = "SELECT ct.id, ct.source, ct.currency, ct.total_amount, ct.invoice_id, ct.contribution_page_id,
                c.first_name, c.last_name, c.preferred_language, e.email,
                cr.payment_processor_id, pp.name processor_name, pp.user_name, pp.password, pp.signature, pp.subject,
                pp.url_site, pp.url_api, pp.is_test
              FROM civicrm_contribution ct
                JOIN civicrm_contribution_recur cr ON cr.id = ct.contribution_recur_id
                JOIN civicrm_payment_processor pp ON pp.id = cr.payment_processor_id
                JOIN civicrm_contact c ON c.id = ct.contact_id
                JOIN civicrm_email e ON e.contact_id = ct.contact_id AND e.is_primary = 1
              WHERE ct.id = %1";
    $params = [
      1 => [$contributionId, 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $params);
    $dao->fetch();
    $result = $dao;
    $dao->free();

    return $result;
  }

  /**
   * Prepare PayU Order params for first payment.
   *
   * @param int $paymentProcessorId
   * @param int $merchantPosId
   * @param int $contributionId
   * @param object $dao
   * @param string $token
   *
   * @return array
   */
  public function prepare($paymentProcessorId, $merchantPosId, $contributionId, $dao, $token) {
    $recurring = self::RECURRING_FIRST;
    $extOrderId = CRM_Payu_Logic_Contribution::encodeIdRecur($contributionId, 1);
    return parent::prepareOrder($paymentProcessorId, $merchantPosId, $extOrderId, $dao, $token, $recurring);
  }

}
