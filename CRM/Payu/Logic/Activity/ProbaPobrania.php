<?php

class CRM_Payu_Logic_Activity_ProbaPobrania {

  /**
   * @param string $cardResponseStatus
   * @param int $contactId
   * @param int $campaignId
   * @param int $contributionId
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function active(string $cardResponseStatus, int $contactId, int $contributionId, int $campaignId = 0) {
    $subject = self::subject($cardResponseStatus);
    return self::create($contactId, $contributionId, $campaignId, CRM_Payu_Model_ActivityStatus::active(), $subject);
  }

  /**
   * @param string $cardResponseStatus
   * @param int $contactId
   * @param int $campaignId
   * @param int $contributionId
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function cancelled(string $cardResponseStatus, int $contactId, int $contributionId, int $campaignId = 0) {
    $subject = self::subject($cardResponseStatus);
    return self::create($contactId, $contributionId, $campaignId, CRM_Payu_Model_ActivityStatus::cancelled(), $subject);
  }

  /**
   * @param string $cardResponseStatus
   * @param int $contactId
   * @param int $campaignId
   * @param int $contributionId
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function failed(string $cardResponseStatus, int $contactId, int $contributionId, int $campaignId = 0) {
    $subject = self::subject($cardResponseStatus);
    return self::create($contactId, $contributionId, $campaignId, CRM_Payu_Model_ActivityStatus::failed(), $subject);
  }

  /**
   * @param string $cardResponseStatus
   * @param int $contactId
   * @param int $campaignId
   * @param int $contributionId
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function closed(string $cardResponseStatus, int $contactId, int $contributionId, int $campaignId = 0) {
    $subject = self::subject($cardResponseStatus);
    return self::create($contactId, $contributionId, $campaignId, CRM_Payu_Model_ActivityStatus::closed(), $subject);
  }

  /**
   * @param $cardResponseStatus
   * @return string
   */
  private static function subject($cardResponseStatus) {
    $label = CRM_Payu_Model_CardStatus::$labels[$cardResponseStatus];

    return sprintf('Kod odpowiedzi:%s %s', $cardResponseStatus, $label);
  }

  /**
   * @param int $contactId
   * @param int $contributionId
   * @param int $campaignId
   * @param $statusId
   * @param $subject
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  private static function create(int $contactId, int $contributionId, int $campaignId, $statusId, $subject) {
    $params = [
      'sequential' => 1,
      'source_contact_id' => $contactId,
      'source_record_id' => $contributionId,
      'activity_type_id' => CRM_Payu_Model_ActivityType::probaPobraniaDeklaracjiCyklicznej(),
      'activity_date_time' => date('YmdHis'),
      'status_id' => $statusId,
      'subject' => $subject,
      'api.ActivityContact.create' => [
        0 => [
          'activity_id' => '$value.id',
          'contact_id' => $contactId,
          'record_type_id' => 3,
        ],
      ],
    ];
    if ($campaignId) {
      $params['campaign_id'] = $campaignId;
    }
    $result = civicrm_api3('Activity', 'create', $params);

    return $result['id'];

  }

}
