<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Logic_Activity_DeklaracjaPlatnosci extends CRM_Payu_Logic_Activity {

  /**
   * Create new activity Deklaracja płatności cyklicznej.
   *
   * @param int $contactId
   * @param int $campaignId
   * @param int $recurId
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function createNew(int $contactId, int $campaignId, int $recurId) {
    $id = self::find($recurId, CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej());
    if ($id) {
      return $id;
    }
    $params = [
      'sequential' => 1,
      'source_contact_id' => $contactId,
      'source_record_id' => $recurId,
      'activity_type_id' => CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(),
      'activity_date_time' => date('YmdHis'),
      'status_id' => CRM_Payu_Model_ActivityStatus::new(),
      'subject' => E::ts('Contribution ID') . ': ' . $recurId,
      'details' => self::getRecurringPaymentUrl($recurId),
      'api.ActivityContact.create' => [
        0 => [
          'activity_id' => '$value.id',
          'contact_id' => $contactId,
          'record_type_id' => 3,
        ],
      ],
    ];
    if ($campaignId) {
      $params['campaign_id'] = $campaignId;
    }
    $result = civicrm_api3('Activity', 'create', $params);

    return $result['id'];
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setActive(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(),
      CRM_Payu_Model_ActivityStatus::active());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setCancelled(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(),
      CRM_Payu_Model_ActivityStatus::cancelled());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setFailed(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(),
      CRM_Payu_Model_ActivityStatus::failed());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setClosed(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(),
      CRM_Payu_Model_ActivityStatus::closed());
  }

}
