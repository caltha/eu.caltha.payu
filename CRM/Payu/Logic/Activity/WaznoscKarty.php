<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Logic_Activity_WaznoscKarty extends CRM_Payu_Logic_Activity {

  /**
   * Create new activity Ważność karty kredytowej.
   *
   * @param int $contactId
   * @param int $campaignId
   * @param int $recurId
   * @param int $expirationMonth
   * @param int $expirationYear
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function createNew(int $contactId, int $campaignId, int $recurId, int $expirationMonth, int $expirationYear) {
    $id = self::find($recurId, CRM_Payu_Model_ActivityType::waznoscKartyKredytowej());
    if ($id) {
      return $id;
    }
    $params = [
      'sequential' => 1,
      'source_contact_id' => $contactId,
      'source_record_id' => $recurId,
      'activity_type_id' => CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(),
      'activity_date_time' => self::expirationDate($expirationMonth, $expirationYear),
      'status_id' => CRM_Payu_Model_ActivityStatus::new(),
      'subject' => E::ts('Contribution ID') . ': ' . $recurId,
      'details' => self::getRecurringPaymentUrl($recurId),
      'api.ActivityContact.create' => [
        0 => [
          'activity_id' => '$value.id',
          'contact_id' => $contactId,
          'record_type_id' => 3,
        ],
      ],
    ];
    if ($campaignId) {
      $params['campaign_id'] = $campaignId;
    }
    $result = civicrm_api3('Activity', 'create', $params);

    return $result['id'];
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setActive(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(),
      CRM_Payu_Model_ActivityStatus::active());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setCancelled(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(),
      CRM_Payu_Model_ActivityStatus::cancelled());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setFailed(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(),
      CRM_Payu_Model_ActivityStatus::failed());
  }

  /**
   * @param int $recurId
   *
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public static function setClosed(int $recurId) {
    return self::setStatus($recurId,
      CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(),
      CRM_Payu_Model_ActivityStatus::closed());
  }

  /**
   * Calculate short date of expiration date of credit card.
   *
   * @param int $expirationMonth
   * @param int $expirationYear
   *
   * @return string
   */
  private static function expirationDate(int $expirationMonth, int $expirationYear) {
    $dt = DateTime::createFromFormat('Y-n', $expirationYear . '-' . $expirationMonth);
    return $dt->format('Y-m-t');
  }

}
