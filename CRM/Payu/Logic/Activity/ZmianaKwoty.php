<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Logic_Activity_ZmianaKwoty extends CRM_Payu_Logic_Activity {


	public static function saveActivity($contributeID, $contactID, $initialAmount, $newAmount, $contributionUrl, $currency){
		$results = \Civi\Api4\Activity::create(FALSE)
			->addValue('activity_type_id', CRM_Payu_Model_ActivityType::zmianaStalejWplaty())
			->addValue('source_contact_id', 'user_contact_id')
			->addValue('subject', 'Zmiana kwoty stałej')
			->addValue('target_contact_id', [
				$contactID,
			])
			->addValue('details', sprintf('Zmiana kwoty %3$s z %1$s %4$s na kwotę %2$s %4$s', $initialAmount, $newAmount, $contributionUrl,$currency) )
			->execute();
		return $results;

	}
	/**
   * Method return url for recurring payment page
   *
   * @param int $recurId
   */
  public static function getRecurringPaymentUrl(int $recurId) {
    $url = CRM_Utils_System::url("civicrm/contact/view/contributionrecur", [
      'reset' => 1,
      'id' => $recurId,
    ]);
    return '<a href="' . $url . '">' . E::ts('stałej wpłaty') . '</a>';
  }
}
