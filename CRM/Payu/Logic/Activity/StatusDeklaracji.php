<?php

class CRM_Payu_Logic_Activity_StatusDeklaracji {

  /**
   * @param string $cardResponseStatus
   * @param array $recurring
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function cancelled(string $cardResponseStatus, array $recurring) {
    return self::create(
      $cardResponseStatus,
      $recurring,
      CRM_Payu_Model_ActivityType::deklaracjaPayUAnulowanie()
    );
  }

  /**
   * @param string $cardResponseStatus
   * @param array $recurring
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function failed(string $cardResponseStatus, array $recurring) {
    return self::create(
      $cardResponseStatus,
      $recurring,
      CRM_Payu_Model_ActivityType::deklaracjaPayUBlad()
    );
  }

  /**
   * @param string $cardResponseStatus
   * @param array $recurring
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function closed(string $cardResponseStatus, array $recurring) {
    return self::create(
      $cardResponseStatus,
      $recurring,
      CRM_Payu_Model_ActivityType::deklaracjaPayUZamkniecie(),
    );
  }

  /**
   * @param $cardResponseStatus
   * @return string
   */
  private static function subject($cardResponseStatus): string {
    return sprintf('Kod odpowiedzi:%s %s', $cardResponseStatus, CRM_Payu_Model_CardStatus::$labels[$cardResponseStatus]);
  }

  /**
   * @param string $cardResponseStatus
   * @param array $recurring
   * @param int $activityTypeId
   * @return mixed
   * @throws CRM_Core_Exception
   */
  private static function create(string $cardResponseStatus, array $recurring, int $activityTypeId) {

    $activity = \Civi\Api4\Activity::create(FALSE)
      ->addValue('sequential', 1)
      ->addValue('source_contact_id', $recurring['contact_id'])
      ->addValue('target_contact_id', $recurring['contact_id'])
      ->addValue('source_record_id', $recurring['id'])
      ->addValue('activity_type_id', $activityTypeId)
      ->addValue('status_id', CRM_Core_PseudoConstant::getKey('CRM_Activity_BAO_Activity', 'status_id', 'Completed'))
      ->addValue('payu_zdarzenie.status_pobrania', $cardResponseStatus)
      ->addValue('subject', self::subject($cardResponseStatus));

    $campaignId = (int)$recurring['campaign_id'];
    if ($campaignId) {
      $activity = $activity->addValue('campaign_id', $campaignId);
    }

    return $activity->execute()->single()['id'];
  }

}
