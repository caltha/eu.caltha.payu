<?php

class CRM_Payu_Logic_Response {

  /**
   * Żądanie zostało wykonane poprawnie.
   */
  const SUCCESS = 'SUCCESS';

  /**
   * Wymagane podanie kod CVV2/CVC2. Wywołaj metodę OpenPayU.authorizeCVV().
   */
  const WARNING_CONTINUE_CVV = 'WARNING_CONTINUE_CVV';

  /**
   * Wymagana autoryzacja 3DS. Należy wykonać przekierowanie w celu kontynuacji
   * procesu płatności (można skorzystać z metody OpenPayU.authorize3DS()).
   */
  const WARNING_CONTINUE_3DS = 'WARNING_CONTINUE_3DS';

  /**
   * Żądanie wykonano poprawnie. Parametr redirectUri został przekazany
   * w nagłówku Location i w treści odpowiedzi w formacie JSON.
   * Dotyczy transparentnej integracji z użyciem sekcji payMethods
   * i zamówienia z użyciem metod płatności: orx, bnx, gbx, nlx.
   */
  const WARNING_CONTINUE_REDIRECT = 'WARNING_CONTINUE_REDIRECT';

  /**
   * System PayU jest niedostępny. Spróbuj ponownie później.
   */
  const BUSINESS_ERROR = 'BUSINESS_ERROR';

  /**
   * System PayU jest niedostępny. Spróbuj ponownie później.
   */
  const ERROR_INTERNAL = 'ERROR_INTERNAL';

  /**
   * System PayU jest niedostępny. Spróbuj ponownie później.
   */
  const GENERAL_ERROR = 'GENERAL_ERROR';

  /**
   * Wystąpił drobny niespodziewany błąd. Spróbuj ponownie później.
   */
  const WARNING = 'WARNING';

  /**
   * System PayU jest niedostępny. Spróbuj ponownie później.
   */
  const SERVICE_NOT_AVAILABLE = 'SERVICE_NOT_AVAILABLE';

  /**
   * W systemie PayU brak danych, które wskazano w żądaniu.
   */
  const DATA_NOT_FOUND = 'DATA_NOT_FOUND';

  /**
   * Zamówienie już zostało utworzone. Ten błąd może występować w sytuacji,
   * w której podano nieunikalny parametr extOrderId.
   */
  const ERROR_ORDER_NOT_UNIQUE = 'ERROR_ORDER_NOT_UNIQUE';

  /**
   * Błędne uwierzytelnienie. Należy sprawdzić parametry podpisu
   * i prawidłowość wdrożenia algorytmu podpisu.
   */
  const UNAUTHORIZED = 'UNAUTHORIZED';

  /**
   * Brak uprawnień do wykonania żądania.
   */
  const UNAUTHORIZED_REQUEST = 'UNAUTHORIZED_REQUEST';

  /**
   * Upłynął okres ważności dla realizacji żądania.
   */
  const TIMEOUT = 'TIMEOUT';

  /**
   * Brakuje jednej lub więcej wartości.
   */
  const ERROR_VALUE_MISSING = 'ERROR_VALUE_MISSING';

  /**
   * Jedna lub więcej wartości jest nieprawidłowa.
   */
  const ERROR_VALUE_INVALID = 'ERROR_VALUE_INVALID';

  /**
   * Błędna składnia żądania.
   */
  const ERROR_SYNTAX = 'ERROR_SYNTAX';

  /**
   * Statusy, po których można ponownie pobrać płatność.
   *
   * @var array
   */
  public static $retry = [
    self::BUSINESS_ERROR,
    self::ERROR_INTERNAL,
    self::GENERAL_ERROR,
    self::WARNING,
    self::SERVICE_NOT_AVAILABLE,
  ];

}
