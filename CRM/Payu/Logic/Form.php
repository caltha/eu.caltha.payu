<?php

require_once __DIR__ . '/../../../packages/vendor/autoload.php';

class CRM_Payu_Logic_Form {

  /**
   * Prepare form.
   *
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param array $processor
   *
   * @return array
   */
  public static function prepare($propertyBag, $processor) {
    $request = [
      'merchant-pos-id' => OpenPayU_Configuration::getMerchantPosId(),
      'shop-name' => $processor['name'],
      'total-amount' => $propertyBag->getAmount(),
      'currency-code' => $propertyBag->getCurrency(),
      'customer-language' => "pl",
      'store-card' => "true",
      'recurring-payment' => "true",
      'customer-email' => $propertyBag->getEmail(),
      'widget-mode' => 'pay',
    ];
    return $request;
  }

  /**
   * Calculate signature hash for form.
   *
   * @param array $form
   * @param string $algorithm
   *
   * @return string
   */
  public static function signature($form, $algorithm = 'sha256') {
    ksort($form);
    $signatureKey = OpenPayU_Configuration::getSignatureKey();
    return hash($algorithm, implode('', $form) . $signatureKey);
  }

  /**
   * Extend form with additional params.
   *
   * @param array $form
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param string $signature
   * @param string $urlSite
   *
   * @return mixed
   */
  public static function extend($form, $propertyBag, $signature, $urlSite) {
    $form['contribution-id'] = $propertyBag->getContributionID();
    $form['sig'] = $signature;
    $form['url-site'] = $urlSite;
    return $form;
  }

}
