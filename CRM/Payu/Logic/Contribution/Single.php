<?php

class CRM_Payu_Logic_Contribution_Single extends CRM_Payu_Logic_Contribution {

  public function __construct() {
    parent::__construct();
  }

  public function find($contributionId, $invoiceId = '') {
    $contribution = new CRM_Contribute_BAO_Contribution();
    $contribution->id = $contributionId;
    if ($invoiceId) {
      $contribution->invoice_id = $invoiceId;
    }
    $contribution->find();
    $contribution->fetch();
    return $contribution;
  }

  /**
   * Payment.create changes status of contribution to Completed
   *
   * @param $contributionId
   * @param $totalAmount
   * @param int $paymentId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function complete($contributionId, $totalAmount, $paymentId = 0) {
    $params = [
      'contribution_id' => $contributionId,
      'total_amount' => $totalAmount,
      'trxn_date' => date('YmdHis'),
      'is_send_contribution_notification' => 0,
    ];
    if ($paymentId) {
      $params['trxn_id'] = $paymentId;
    }
    $result = civicrm_api3('Payment', 'create', $params);
  }

  /**
   * Cancel contribution by using Order.cancel api action
   *
   * @param $contributionId
   * @param int $paymentId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function cancel($contributionId, $paymentId = 0) {
    $params = [
      'id' => $contributionId,
    ];
    if ($paymentId) {
      $params['trxn_id'] = $paymentId;
    }
    $result = civicrm_api3('Order', 'cancel', $params);
  }

  /**
   * Set contribution as Failed using Contribution.create api action.
   *
   * fixme api Order has only cancel action, not failed
   *
   * @param $contributionId
   * @param int $paymentId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function failed($contributionId, $paymentId = 0) {
    $contributionStatuses = CRM_Contribute_PseudoConstant::contributionStatus(NULL, 'name');
    $params = [
      'id' => $contributionId,
      'contribution_status_id' => array_search('Failed', $contributionStatuses),
    ];
    if ($paymentId) {
      $params['trxn_id'] = $paymentId;
    }
    $result = civicrm_api3('Contribution', 'create', $params);
  }

  /**
   * Set financial type after contribution page on single donation.
   *
   * @param $contributionId
   * @param $financialTypeId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function setFinancialType($contributionId, $financialTypeId) {
    $params = [
      'sequential' => 1,
      'id' => $contributionId,
      'financial_type_id' => $financialTypeId,
    ];
    civicrm_api3('Contribution', 'create', $params);
  }

  /**
   * @param $contributionId
   * @param $note
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function addNote($contributionId, $note) {
    $result = civicrm_api3('Note', 'create', [
      'subject' => 'Added by PayU',
      'entity_table' => "civicrm_contribution",
      'entity_id' => $contributionId,
      'note' => $note,
    ]);
  }

}
