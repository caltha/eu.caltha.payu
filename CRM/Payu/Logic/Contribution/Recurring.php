<?php

class CRM_Payu_Logic_Contribution_Recurring extends CRM_Payu_Logic_Contribution {

  public function __construct() {
    parent::__construct();
  }

  /**
   * @param $contributionRecurId
   * @return mixed
   * @throws CiviCRM_API3_Exception
   */
  public function find($contributionRecurId) {
    $params = [
      'sequential' => 1,
      'id' => $contributionRecurId,
    ];
    $result = civicrm_api3('ContributionRecur', 'get', $params);

    return $result['values'][0];
  }

  /**
   * Set recurring contribution as In progress.
   *
   * @param int $contributionRecurId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function inProgress($contributionRecurId) {
    $params = [
      'sequential' => 1,
      'id' => $contributionRecurId,
      'contribution_status_id' => $this->inProgress,
    ];

    $result = civicrm_api3('ContributionRecur', 'create', $params);
  }

  /**
   * Set recurring contribution as Completed.
   *
   * @param int $contributionRecurId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function complete(int $contributionRecurId) {
    $params = [
      'sequential' => 1,
      'id' => $contributionRecurId,
      'contribution_status_id' => $this->completed,
      'end_date' => date('YmdHis'),
    ];

    $result = civicrm_api3('ContributionRecur', 'create', $params);
  }

  /**
   * todo change to api action ContributionRecur.cancel
   *  add .cancel_reason -> goes to contribution_recur?
   *  .processor_message -> goes to activity.details of Cancel contribution
   *
   * @deprecated
   *
   * @param int $contributionRecurId
   */
  public function cancel($contributionRecurId) {
    $recur = new CRM_Contribute_BAO_ContributionRecur();
    $recur->id = $contributionRecurId;
    $recur->contribution_status_id = $this->cancelled;
    $recur->save();
  }

  /**
   * Set recurring contribution as Failed.
   *
   * @param int $contributionRecurId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function failed($contributionRecurId) {
    $params = [
      'sequential' => 1,
      'id' => $contributionRecurId,
      'contribution_status_id' => $this->failed,
      'cancel_date' => date('YmdHis'),
    ];

    $result = civicrm_api3('ContributionRecur', 'create', $params);
  }

  /**
   * Save recurring token.
   *
   * @param int $contributionRecurId
   * @param string $token
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function saveToken($contributionRecurId, $token) {
    $params = [
      'sequential' => 1,
      'id' => $contributionRecurId,
      'trxn_id' => $token . '.' . $contributionRecurId,
      'cycle_day' => $this->cycleDay(),
    ];

    $result = civicrm_api3('ContributionRecur', 'create', $params);
  }

  /**
   * Calculate cycle day
   * @return int
   */
  private function cycleDay() {
    $day = (int) date('j');
    if ($day > 28) {
      return 28;
    }

    return $day;
  }

}
