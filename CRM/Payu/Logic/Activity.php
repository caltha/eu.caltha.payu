<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Logic_Activity {

  /**
   * @param int $recurId
   *
   * @param string $activityTypeId
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  protected static function find(int $recurId, string $activityTypeId) {
    $params = [
      'sequential' => 1,
      'source_record_id' => $recurId,
      'activity_type_id' => $activityTypeId,
      'options' => ['limit' => 1],
    ];
    $result = civicrm_api3('Activity', 'get', $params);
    if ($result['count'] == 1) {
      return $result['id'];
    }

    return 0;
  }

  /**
   * @param int $recurId
   * @param string $activityTypeId
   * @param string $statusId
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  protected static function setStatus(int $recurId, string $activityTypeId, string $statusId) {
    $id = self::find($recurId, $activityTypeId);
    if ($id) {
      $params = [
        'sequential' => 1,
        'id' => $id,
        'status_id' => $statusId,
      ];
      $result = civicrm_api3('Activity', 'create', $params);

      return !$result['is_error'];
    }

    return FALSE;
  }

  /**
   * Method return url for recurring payment page
   *
   * @param int $recurId
   */
  public static function getRecurringPaymentUrl(int $recurId) {
    $url = CRM_Utils_System::url("civicrm/contact/view/contributionrecur", [
      'reset' => 1,
      'id' => $recurId,
    ]);
    return '<a href="' . $url . '">' . E::ts('Recurring Contribution') . '</a>';
  }

}
