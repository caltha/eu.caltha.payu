<?php

class CRM_Payu_Logic_Card {

  public function saveCardStatus($contributionId, $responseCode, $attempt) {
    $card = $this->findCardStatus($contributionId);
    if ($card->N) {
      $query = $this->buildUpdate($contributionId, $responseCode, $attempt);
    }
    else {
      $query = $this->buildInsert($contributionId, $responseCode, $attempt);
    }
    CRM_Core_DAO::executeQuery($query);
  }

  private function findCardStatus($contributionId) {
    $query = "SELECT * FROM civicrm_value_payu_contribution WHERE entity_id = %1";
    $params = [
      1 => [$contributionId, 'Integer'],
    ];
    $dao = CRM_Core_DAO::executeQuery($query, $params);
    $dao->fetch();
    $result = $dao;
    $dao->free();

    return $result;
  }

  private function buildUpdate($contributionId, $responseCode, $attempt) {
    $query = "UPDATE civicrm_value_payu_contribution SET
                status_" . $attempt . "_proby_pobrania = '" . $responseCode . "',
                data_" . $attempt . "_proby_pobrania = NOW() ";
    if ($responseCode == CRM_Payu_Model_CardStatus::OK_000 && $attempt < 3) {
      for ($i = $attempt + 1; $i <= 3; $i++) {
        $query .= ", status_" . $i . "_proby_pobrania = '" . CRM_Payu_Model_CardStatus::NOT_APPLICABLE . "',
          data_" . $i . "_proby_pobrania = NOW()";
      }
    }
    $query .= " WHERE entity_id = " . $contributionId;

    return $query;
  }

  private function buildInsert($contributionId, $responseCode, $attempt) {
    $query = "INSERT INTO civicrm_value_payu_contribution (entity_id,
                status_" . $attempt . "_proby_pobrania,
                data_" . $attempt . "_proby_pobrania";
    if ($responseCode == CRM_Payu_Model_CardStatus::OK_000 && $attempt < 3) {
      for ($i = $attempt + 1; $i <= 3; $i++) {
        $query .= ", status_" . $i . "_proby_pobrania,
          data_" . $i . "_proby_pobrania";
      }
    }
    $query .= ") VALUES (" . $contributionId . ", '" . $responseCode . "', NOW()";
    if ($responseCode == CRM_Payu_Model_CardStatus::OK_000 && $attempt < 3) {
      for ($i = $attempt + 1; $i <= 3; $i++) {
        $query .= ", '" . CRM_Payu_Model_CardStatus::NOT_APPLICABLE . "', NOW()";
      }
    }
    $query .= ")";

    return $query;
  }

}
