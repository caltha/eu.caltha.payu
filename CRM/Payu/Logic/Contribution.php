<?php

class CRM_Payu_Logic_Contribution {

  const TYPE_RECURRING = 'R';
  const TYPE_SINGLE = 'S';

  public $type = '';
  public $attempt = 0;
  public $id = 0;

  protected $pending = 0;
  protected $completed = 0;
  protected $inProgress = 0;
  protected $cancelled = 0;
  protected $failed = 0;

  public function __construct() {
    $this->pending = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Pending');
    $this->completed = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Completed');
    $this->inProgress = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'In Progress');
    $this->cancelled = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Cancelled');
    $this->failed = CRM_Core_PseudoConstant::getKey('CRM_Contribute_BAO_Contribution', 'contribution_status_id', 'Failed');
  }

  /**
   * Check if given contribution is first in recurring
   *
   * @param int $id
   *
   * @return bool
   */
  public function isFirst($id) {
    $query = "SELECT t.id
              FROM
                (SELECT ct1.id
                FROM civicrm_contribution ct1
                WHERE contribution_recur_id =
                    (SELECT contribution_recur_id
                    FROM civicrm_contribution ct
                    WHERE ct.id = %1) AND ct1.contribution_recur_id IS NOT NULL
                ORDER BY ct1.id
                LIMIT 1) t
              WHERE t.id = %1";
    $params = [
      1 => [$id, 'Integer'],
    ];
    $isFirst = CRM_Core_DAO::singleValueQuery($query, $params);

    return (bool) $isFirst;
  }

  /**
   * Decode external id from format ID.TYPE.ATTEMPT.HASH
   * HASH part is omitted
   *
   * @param string $extOrderId
   */
  public function decodeId($extOrderId) {
    $t = explode('.', $extOrderId);
    $this->id = $t[0];
    $this->type = $t[1];
    $this->attempt = $t[2];
  }

  /**
   * Encode contribution id for single type.
   *
   * @param $id
   * @param $attempt
   *
   * @return string
   */
  public static function encodeIdSingle($id, $attempt) {
    return self::encodeId($id, self::TYPE_SINGLE, $attempt);
  }

  /**
   * Encode contribution id for recurring type.
   *
   * @param $id
   * @param $attempt
   *
   * @return string
   */
  public static function encodeIdRecur($id, $attempt) {
    return self::encodeId($id, self::TYPE_RECURRING, $attempt);
  }

  private static function encodeId($id, $type, $attempt) {
    return implode('.', [$id, $type, $attempt, self::encodeIdHash($id)]);
  }

  private static function encodeIdHash($id) {
    return hash("crc32", $id . time());
  }
  /**
   * Set PayU Payment id
   *
   * @param $contributionId
   * @param $paymentId
   *
   * @throws \CiviCRM_API3_Exception
   */
  public static function setPaymentId($contributionId, $paymentId) {
    if ($paymentId) {
      $params = [
        'contribution_id' => $contributionId,
        'trxn_id' => $paymentId,
      ];
      $result = civicrm_api3('Contribution', 'create', $params);
    }
  }

  /**
   * @param int $id
   * @return int
   */
  public function findRecurringId(int $id) {
    $query = "SELECT contribution_recur_id FROM civicrm_contribution ct WHERE ct.id = %1";
    $params = [
      1 => [$id, 'Integer'],
    ];

    return (int) CRM_Core_DAO::singleValueQuery($query, $params);
  }

}
