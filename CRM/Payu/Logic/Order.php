<?php

class CRM_Payu_Logic_Order {

  /**
   * Value only for first order in cycle
   */
  const RECURRING_FIRST = 'FIRST';

  /**
   * Value for second and next orders in cycle
   */
  const RECURRING_STANDARD = 'STANDARD';

  /**
   * Supported method type
   */
  const PAYMENT_METHOD_TYPE = 'CARD_TOKEN';

  /**
   * Prepare PayU Order params.
   *
   * @param int $paymentProcessorId
   * @param int $merchantPosId
   * @param int $extOrderId
   * @param object $dao
   * @param string $token
   * @param string $recurring
   *
   * @return array
   */
  protected function prepareOrder($paymentProcessorId, $merchantPosId, $extOrderId, $dao, $token = '', $recurring = '') {
    $order = [
      'notifyUrl' => CRM_Utils_System::url('civicrm/payu/notify-recur', ['pp' => $paymentProcessorId], TRUE),
      // todo dlaczego CRM_Utils_System::url nieprawidłowo skleja parametry? &amp; zamiast &
      'continueUrl' => CRM_Utils_System::url('civicrm/payu/summary-recur', $this->urlParams($dao, $paymentProcessorId), TRUE),
      'customerIp' => $_SERVER['REMOTE_ADDR'],
      'merchantPosId' => $merchantPosId,
      'recurring' => $recurring,
      'description' => $dao->source,
      'currencyCode' => $dao->currency,
      'totalAmount' => self::amount($dao->total_amount),
      'extOrderId' => $extOrderId,
      'products' => [
        0 => [
          'name' => 'darowizna cykliczna',
          'unitPrice' => self::amount($dao->total_amount),
          'quantity' => 1,
        ],
      ],
      'buyer' => [
        'email' => $dao->email,
        'firstName' => $dao->first_name,
        'lastName' => $dao->last_name,
        'language' => substr($dao->preferred_language, 0, 2),
      ],
    ];
    if ($token) {
      $order['payMethods'] = [
        'payMethod' => [
          'type' => self::PAYMENT_METHOD_TYPE,
          'value' => $token,
        ],
      ];
    }
    if (!$recurring) {
      unset($order['recurring']);
      $order['notifyUrl'] = CRM_Utils_System::url('civicrm/payu/notify-single', ['pp' => $paymentProcessorId], TRUE);
      $order['continueUrl'] = CRM_Utils_System::url('civicrm/payu/summary-single', $this->urlParams($dao, $paymentProcessorId), TRUE);
    }

    return $order;
  }

  /**
   * Convert amount to minor currency unit.
   *
   * @param $amount
   *
   * @return int
   */
  private static function amount($amount) {
    return (int) str_replace(['.', ',', ' '], '', $amount);
  }

  /**
   * @param object $dao
   * @param int $paymentProcessorId
   *
   * @return array
   */
  private function urlParams($dao, $paymentProcessorId) {
    $urlParams = [
      'id' => $dao->id,
      'pp' => $paymentProcessorId,
    ];
    if (property_exists($dao, 'invoice_id')) {
      $urlParams['invoice_id'] = $dao->invoice_id;
    }
    return $urlParams;
  }


  /**
   * Save orderId from PayU to invoice_number field.
   *
   * @param int $contributionId
   * @param string $invoiceNumber
   *
   * @throws \CiviCRM_API3_Exception
   */
  public function saveOrderId($contributionId, $invoiceNumber) {
    $params = [
      'sequential' => 1,
      'id' => $contributionId,
      'invoice_number' => $invoiceNumber,
    ];
    civicrm_api3('Contribution', 'create', $params);
  }

}
