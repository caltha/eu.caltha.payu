<?php

use CRM_Payu_ExtensionUtil as E;
use Civi\Api4\Generic;
use Civi\Payment\Exception\PaymentProcessorException;

/**
 * Form controller class
 *
 * @see https://docs.civicrm.org/dev/en/latest/framework/quickform/
 */
class CRM_Payu_Form_ChangeAmount extends CRM_Contribute_Form_ContributionRecur {

	public $initialAmount;
	public $currency;

	public function permissions(){
		$show = false;
		$contributionID = $this->getContributionRecurID();
		$contributionRecurs = \Civi\Api4\ContributionRecur::get(FALSE)
      ->addSelect('payment_processor_id.payment_processor_type_id:name','contribution_status_id:name')
      ->addWhere('id', '=', $contributionID)
      ->execute();

    if($contributionRecurs[0]['payment_processor_id.payment_processor_type_id:name'] == 'Payu') {
      $possibleStatuses = array('Pending', 'In Progress');
      foreach ($possibleStatuses as $status) {
        if ($status == $contributionRecurs[0]['contribution_status_id:name']) {
          $show = true;
        }
      }
	}
	  return $show;
	}


  /**
   * @throws \CRM_Core_Exception
   */


	public function buildQuickForm(): void {
      $this->setTitle('Zmiana kwoty');
		if($this->permissions() == true){
		// add form elements
		$amtAttr = ['size' => 20];
    $this->addMoney('amount', ts('Wpisz kwote'), TRUE, $amtAttr,
      FALSE, 'currency123', $this->currency, TRUE
    );
    $this->addCurrency(
      'currency123',
      NULL,
      FALSE,
      $this->currency,
      TRUE,
      TRUE
    );

		//$addCurrency = TRUE, string $currencyName = 'currency', null $defaultCurrency = NULL, bool $freezeCurrency = FALSE)

		$this->addButtons([
		  [
			'type' => 'submit',
			'name' => E::ts('Zmień kwotę'),
			'spacing' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
			'isDefault' => TRUE,
		  ],
		  [
			'type' => 'cancel',
			'name' => E::ts('Not Now'),
		  ],
		]);

		// export form elements
		$this->assign('elementNames', $this->getRenderableElementNames());
    parent::buildQuickForm();
      }
	}
	
	
	public function preProcess() {
    $contributionID = $this->getContributionRecurID();
    if($this->permissions() == true){
        parent::preProcess();

        $contactID = $this->getSubscriptionContactID();
        $getContributionRecurData = $this->getContributionRecurData($contributionID, $contactID);
        $this->initialAmount = $getContributionRecurData['amount'];
        $this->currency = $getContributionRecurData['currency'];
        $this->assign('contributionRecurData', $getContributionRecurData);

        $this->assign('contactId', $this->getSubscriptionContactID());
        $this->assign('membershipID', $this->getMembershipID());
      }
	}
    public function addRule($element, $message, $type, $format = null, $validation = 'server', $reset = false, $force = false){
    $this->addFormRule(array(get_class($this), 'valudateAmount'));
  }

  public static function valudateAmount($values){
    $error = array();
    if(!is_numeric($values['amount'])){
      $error['amount'] = ts('Wpisz prawidłową kwotę');
    }elseif($values['amount'] <= 0){
      $error['amount'] = ts('Kwota musi być wieksza od 0');
    }
    return empty($error) ? TRUE : $error;
}

	public function postProcess(): void {

		$values = $this->exportValues();
		$amount =  $values['amount'];
		$contributionID	= $this->getContributionRecurID();
		$contactID = $this->getSubscriptionContactID();


		\Civi\Api4\ContributionRecur::update()
			->addWhere('id', '=', $contributionID)
			->addWhere('contact_id','=',$contactID)
			->addValue('amount',$amount)
			->execute();
		$contributionUrl = CRM_Payu_Logic_Activity_ZmianaKwoty::getRecurringPaymentUrl($contributionID);
		CRM_Payu_Logic_Activity_ZmianaKwoty::saveActivity($contributionID, $contactID, $this->initialAmount, $amount, $contributionUrl, $this->currency);

    $amountFormat = Civi::format()->money($amount, $this->currency,'it_IT');
    $label = sprintf('Kwota stałej wpłaty została zmieniona na %s',$amountFormat);
    CRM_Core_Session::setStatus($label, 'Zgłoszenie podopiecznego', 'success', ['expires' => 0]);
  }

	public function setDefaultValues() {
		$this->_defaults = [];
		$this->_defaults['amount'] = $this->getSubscriptionDetails()->amount;

		return $this->_defaults;
	}

	public function getContributionRecurData($crid, $cid) {
		$result = \Civi\Api4\ContributionRecur::get()
			->addWhere('id', '=', $crid)
			->addWhere('contact_id','=', $cid)
			->addSelect('amount','start_date','currency')
			->execute()->first();
		return $result;
	}

  /**
   * Get the fields/elements defined in this form.
   *
   * @return array (string)
   */
  public function getRenderableElementNames(): array {

    // The _elements list includes some items which should not be
    // auto-rendered in the loop -- such as "qfKey" and "buttons".  These
    // items don't have labels.  We'll identify renderable by filtering on
    // the 'label'.
    $elementNames = [];
    foreach ($this->_elements as $element) {
      /** @var HTML_QuickForm_Element $element */
      $label = $element->getLabel();
      if (!empty($label)) {
        $elementNames[] = $element->getName();
      }
    }
    return $elementNames;
	}
}
