<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Page_Summary_Recur extends CRM_Core_Page {

  /**
   * @return null|void
   * @throws \CRM_Core_Exception
   * @throws \OpenPayU_Exception
   * @throws \OpenPayU_Exception_Configuration
   */
  public function run() {
    CRM_Utils_System::setTitle('');
    $id = CRM_Utils_Request::retrieve('id', 'Integer', $this, TRUE);
    $paymentProcessorId = CRM_Utils_Request::retrieve('pp', 'Integer', $this, TRUE);
    $invoiceId = CRM_Utils_Request::retrieve('invoice_id', 'String', $this, TRUE);

    $single = new CRM_Payu_Logic_Contribution_Single();
    $data = $single->find($id, $invoiceId);
    CRM_Payu_Config::initById($paymentProcessorId);
    sleep(2);
    $order = OpenPayU_Order::retrieve($data->invoice_number);
    $response = $order->getResponse();
    $orderStatus = $response->orders[0]->status;

    $this->assign('verified', $data->N);
    $this->assign('totalAmount', $data->total_amount);
    $this->assign('currency', $data->currency);
    $this->assign('contributionPageId', $data->contribution_page_id);
    $this->assign('orderStatus', $orderStatus);
    CRM_Payu_Hook::alterSummaryPage($this, __CLASS__, $response);
    parent::run();
  }

}
