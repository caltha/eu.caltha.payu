<?php
require_once __DIR__ . '/../../../../packages/vendor/autoload.php';
require_once 'CRM/Core/Page.php';

class CRM_Payu_Page_Notify_Recur extends CRM_Payu_Page_Notify_Logic {

  /**
   * @throws \OpenPayU_Exception
   * @throws \OpenPayU_Exception_Configuration
   * @throws \CRM_Core_Exception
   * @throws \CiviCRM_API3_Exception
   */
  public function run() {
    $paymentProcessorId = CRM_Utils_Request::retrieve('pp', 'Integer', $this, TRUE);
    CRM_Payu_Config::initById($paymentProcessorId);
    $params = json_decode(file_get_contents('php://input'));
    $paymentId = $params->properties[0]->value ?? null;
    if (!$paymentId) {
      CRM_Payu_Error::debug_var('NO PAYMENT_ID $params', $params, __METHOD__);
      header("HTTP/1.1 200 OK");
      CRM_Utils_System::civiExit();
    }
    $id = new CRM_Payu_Logic_Contribution();
    $id->decodeId($params->order->extOrderId);
    if ($this->isValidSignature()) {
      $transaction = OpenPayU_Order::retrieveTransaction($params->order->orderId);
      $response = $transaction->getResponse();
      $card = new CRM_Payu_Logic_Card();
      $recurringId = $id->findRecurringId($id->id);
      $recur = new CRM_Payu_Logic_Contribution_Recurring();
      $recurring = $recur->find($recurringId);
      switch ($params->order->status) {
        case CRM_Payu_Logic_Notify::STATUS_COMPLETED:
          $cardResponseCode = $response->transactions[0]->card->cardData->cardResponseCode;
          $card->saveCardStatus($id->id, $cardResponseCode, $id->attempt);
          CRM_Payu_Logic_Activity_DeklaracjaPlatnosci::setActive($recurringId);
          CRM_Payu_Logic_Activity_WaznoscKarty::setActive($recurringId);
          if (Civi::settings()->get('payu_extend_history')) {
            CRM_Payu_Logic_Activity_ProbaPobrania::active($cardResponseCode, $recurring['contact_id'], $id->id, (int) $recurring['campaign_id']);
          }
          $this->setComplete($id->id, $paymentId);
          $recur->inProgress($recurringId);
          break;

        case CRM_Payu_Logic_Notify::STATUS_CANCELED:
          $cardResponseCode = $response->transactions[0]->card->cardData->cardResponseCode;
          $card->saveCardStatus($id->id, $cardResponseCode, $id->attempt);
          if (in_array($cardResponseCode, CRM_Payu_Model_CardStatus::$close)) {
            CRM_Payu_Logic_Activity_DeklaracjaPlatnosci::setClosed($recurringId);
            CRM_Payu_Logic_Activity_WaznoscKarty::setClosed($recurringId);
            if (Civi::settings()->get('payu_extend_history')) {
              CRM_Payu_Logic_Activity_ProbaPobrania::closed($cardResponseCode, $recurring['contact_id'], $id->id, (int) $recurring['campaign_id']);
            }
            $this->setFailed($id->id, $paymentId);
            $recur->complete($recurringId);
            CRM_Payu_Logic_Activity_StatusDeklaracji::closed($cardResponseCode, $recurring);
          }
          elseif (in_array($cardResponseCode, CRM_Payu_Model_CardStatus::$fail)) {
            CRM_Payu_Logic_Activity_DeklaracjaPlatnosci::setFailed($recurringId);
            CRM_Payu_Logic_Activity_WaznoscKarty::setFailed($recurringId);
            if (Civi::settings()->get('payu_extend_history')) {
              CRM_Payu_Logic_Activity_ProbaPobrania::failed($cardResponseCode, $recurring['contact_id'], $id->id, (int) $recurring['campaign_id']);
            }
            $this->setFailed($id->id, $paymentId);
            $recur->failed($recurringId);
            CRM_Payu_Logic_Activity_StatusDeklaracji::failed($cardResponseCode, $recurring);
          }
          elseif (in_array($cardResponseCode, CRM_Payu_Model_CardStatus::$cancel)) {
            if (Civi::settings()->get('payu_extend_history')) {
              CRM_Payu_Logic_Activity_ProbaPobrania::cancelled($cardResponseCode, $recurring['contact_id'], $id->id, (int) $recurring['campaign_id']);
            }
            $this->setCancelled($id->id, $paymentId);
            CRM_Payu_Logic_Activity_StatusDeklaracji::cancelled($cardResponseCode, $recurring);
          }
          else {
            if (Civi::settings()->get('payu_extend_history')) {
              CRM_Payu_Logic_Activity_ProbaPobrania::cancelled((string) $cardResponseCode, $recurring['contact_id'], $id->id, (int) $recurring['campaign_id']);
            }
            $this->setCancelled($id->id, $paymentId);
            CRM_Payu_Logic_Activity_StatusDeklaracji::cancelled($cardResponseCode, $recurring);
          }
          break;

        case CRM_Payu_Logic_Notify::STATUS_REJECTED:
          // fixme co robimy?
          CRM_Payu_Error::debug_var('REJECTED $params', $params, __METHOD__);
          CRM_Payu_Error::debug_var('REJECTED $response', $response, __METHOD__);
          break;

        case CRM_Payu_Logic_Notify::STATUS_PENDING:
        case CRM_Payu_Logic_Notify::STATUS_WAITING_FOR_CONFIRMATION:
          CRM_Payu_Logic_Contribution::setPaymentId($id->id, $paymentId);
          break;
      }
    }
    else {
      CRM_Payu_Error::debug_var('INVALID SIGNATURE $params', $params, __METHOD__);
    }
    header("HTTP/1.1 200 OK");
    CRM_Utils_System::civiExit();
  }

}
