<?php
require_once __DIR__ . '/../../../../packages/vendor/autoload.php';
require_once 'CRM/Core/Page.php';

class CRM_Payu_Page_Notify_Single extends CRM_Payu_Page_Notify_Logic {

  /**
   * @throws \OpenPayU_Exception
   * @throws \OpenPayU_Exception_Configuration
   * @throws \CRM_Core_Exception
   * @throws \CiviCRM_API3_Exception
   */
  public function run() {
    $paymentProcessorId = CRM_Utils_Request::retrieve('pp', 'Integer', $this, TRUE);
    CRM_Payu_Config::initById($paymentProcessorId);
    $params = json_decode(file_get_contents('php://input'));
    $paymentId = $params->properties[0]->value ?? null;
    if (!$paymentId) {
      CRM_Payu_Error::debug_var('NO PAYMENT_ID $params', $params, __METHOD__);
      header("HTTP/1.1 200 OK");
      CRM_Utils_System::civiExit();
    }
    $id = new CRM_Payu_Logic_Contribution();
    $id->decodeId($params->order->extOrderId);
    if ($this->isValidSignature()) {
      switch ($params->order->status) {
        case CRM_Payu_Logic_Notify::STATUS_COMPLETED:
          $this->setComplete($id->id, $paymentId);
          break;

        case CRM_Payu_Logic_Notify::STATUS_CANCELED:
        case CRM_Payu_Logic_Notify::STATUS_REJECTED:
          $this->setCancelled($id->id, $paymentId);
          break;

        case CRM_Payu_Logic_Notify::STATUS_PENDING:
        case CRM_Payu_Logic_Notify::STATUS_WAITING_FOR_CONFIRMATION:
          CRM_Payu_Logic_Contribution::setPaymentId($id->id, $paymentId);
          break;
      }
    }
    else {
      CRM_Payu_Error::debug_var('INVALID SIGNATURE $params', $params, __METHOD__);
    }
    header("HTTP/1.1 200 OK");
    CRM_Utils_System::civiExit();
  }

}
