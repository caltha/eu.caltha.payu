<?php
require_once __DIR__ . '/../../../../packages/vendor/autoload.php';
require_once 'CRM/Core/Page.php';

class CRM_Payu_Page_Notify_Logic extends CRM_Core_Page {

  /**
   * Check signature received from PayU
   *
   * @return bool
   */
  protected function isValidSignature() {
    $notify = new CRM_Payu_Logic_Notify();
    list($payuSignature, $algorithm) = $notify->parseSignature($_SERVER['HTTP_OPENPAYU_SIGNATURE']);
    $json = file_get_contents('php://input');
    $expectedSignature = hash($algorithm, $json . OpenPayU_Configuration::getSignatureKey());

    return $payuSignature == $expectedSignature;
  }

  /**
   * @param int $contributionId
   * @param int $paymentId Transaction ID returns in PayU response
   *
   * @throws \CiviCRM_API3_Exception
   */
  protected function setComplete($contributionId, $paymentId) {
    $single = new CRM_Payu_Logic_Contribution_Single();
    $contribution = $single->find($contributionId);
    $single->complete($contribution->id, $contribution->total_amount, $paymentId);
  }

  /**
   * Set contribution as Cancelled
   *
   * @param int $contributionId
   * @param int $paymentId Transaction ID returns in PayU response
   *
   * @throws \CiviCRM_API3_Exception
   */
  protected function setCancelled($contributionId, $paymentId) {
    $single = new CRM_Payu_Logic_Contribution_Single();
    $contribution = $single->find($contributionId);
    $single->cancel($contribution->id, $paymentId);
  }

  /**
   * Set contribution as Failed
   *
   * @param int $contributionId
   * @param string $paymentId Transaction ID returns in PayU response
   *
   * @throws \CiviCRM_API3_Exception
   */
  protected function setFailed(int $contributionId, string $paymentId) {
    $single = new CRM_Payu_Logic_Contribution_Single();
    $contribution = $single->find($contributionId);
    $single->failed($contribution->id, $paymentId);
  }

}
