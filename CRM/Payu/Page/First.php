<?php
require_once __DIR__ . '/../../../packages/vendor/autoload.php';
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Page_First extends CRM_Core_Page {
  /**
   * @return null|void
   * @throws \CRM_Core_Exception
   */
  public function run() {
    CRM_Utils_System::setTitle('');
    Civi::resources()->addVars(
      'payu',
      CRM_Utils_Request::exportValues()
    );
    Civi::resources()->addScriptFile(E::LONG_NAME, 'js/first.js', 10, 'html-header');

    parent::run();
  }
}
