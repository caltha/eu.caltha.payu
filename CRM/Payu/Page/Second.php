<?php
use CRM_Payu_ExtensionUtil as E;

class CRM_Payu_Page_Second extends CRM_Core_Page {

  /**
   * @throws \CRM_Core_Exception
   * @throws \OpenPayU_Exception
   * @throws \Exception
   */
  public function run() {
    CRM_Utils_System::setTitle('');
    $token = CRM_Utils_Request::retrieve('token', 'String', $this, FALSE);
    $contributionId = CRM_Utils_Request::retrieve('contributionID', 'Integer', $this, FALSE);
    $merchantPosId = CRM_Utils_Request::retrieve('merchantPosId', 'Integer', $this, FALSE);
    $first = new CRM_Payu_Logic_Order_First();
    $data = $first->find($contributionId);
    $paramsOrder = $first->prepare($data->payment_processor_id, $merchantPosId, $contributionId, $data, $token);

    CRM_Payu_Config::initByObject($data);
    $order = OpenPayU_Order::create($paramsOrder);
    $status = $order->getStatus();
    $response = $order->getResponse();
    $statusDescription = OpenPayU_Util::statusDesc($status);
    $first->saveOrderId($contributionId, $response->orderId);
    $this->assign('status', $status);
    $this->assign('statusDescription', $statusDescription);
    $this->assign('totalAmount', $data->total_amount);
    $this->assign('contributionPageId', $data->contribution_page_id);

    switch ($status) {
      case CRM_Payu_Logic_Response::SUCCESS:
      case CRM_Payu_Logic_Response::WARNING_CONTINUE_3DS:
      case CRM_Payu_Logic_Response::WARNING_CONTINUE_CVV:
        $payMethod = $response->payMethods->payMethod;
        $tokenRecurring = $payMethod->value;
        $expirationMonth = $payMethod->card->expirationMonth;
        $expirationYear = $payMethod->card->expirationYear;
        $single = new CRM_Payu_Logic_Contribution_Single();
        $contribution = $single->find($contributionId);
        CRM_Payu_Logic_Activity_WaznoscKarty::createNew(
          $contribution->contact_id,
          (int) $contribution->campaign_id,
          $contribution->contribution_recur_id,
          $expirationMonth,
          $expirationYear
        );
        CRM_Payu_Logic_Activity_DeklaracjaPlatnosci::createNew(
          $contribution->contact_id,
          (int) $contribution->campaign_id,
          $contribution->contribution_recur_id
        );
        $recur = new CRM_Payu_Logic_Contribution_Recurring();
        $recur->saveToken($contribution->contribution_recur_id, $tokenRecurring);

        if (property_exists($response, 'redirectUri')) {
          $redirectUri = $order->getResponse()->redirectUri;
          if ($redirectUri) {
            CRM_Utils_System::redirect($redirectUri);
          }
        }
        break;

      default:
        CRM_Payu_Error::debug_var('UNSUPPORTED STATUS $status', $status, __METHOD__);
        CRM_Payu_Error::debug_var('UNSUPPORTED STATUS $response', $response, __METHOD__);
        CRM_Payu_Error::debug_var('UNSUPPORTED STATUS $contributionId', $contributionId, __METHOD__);
        CRM_Payu_Error::debug_var('UNSUPPORTED STATUS $paramsOrder', $paramsOrder, __METHOD__);
    }

    CRM_Payu_Hook::alterSummaryPage($this, __CLASS__, $response);
    parent::run();
  }

}
