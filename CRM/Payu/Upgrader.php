<?php
use CRM_Payu_ExtensionUtil as E;

/**
 * Collection of upgrade steps.
 */
class CRM_Payu_Upgrader extends CRM_Extension_Upgrader_Base {

  /**
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public function install() {
    CRM_Payu_Model_PaymentMethod::payu();
    CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej();
    CRM_Payu_Model_ActivityType::waznoscKartyKredytowej();
    $this->executeCustomDataFile('xml/activity_payu.xml');
    $this->executeCustomDataFile('xml/contribution_payu.xml');

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function postInstall() {
    $callable = array_filter(get_class_methods($this), function($name){
      return substr($name, 0, 8) == 'upgrade_';
    });
    foreach ($callable as $function) {
      call_user_func(array($this, $function));
    }

    return TRUE;
  }

  /**
   * @return bool
   * @throws \CiviCRM_API3_Exception
   */
  public function upgrade_021_invalid_token() {
    $result = civicrm_api3('OptionValue', 'create', [
      'option_group_id' => "status_proby_pobrania",
      'label' => "INV - Invalid Token (Caltha)",
      'value' => "INV",
      'name' => "INV_Invalid_Token",
    ]);

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_022_new_activity_statuses() {
    CRM_Payu_Model_ActivityStatus::new();
    CRM_Payu_Model_ActivityStatus::active();
    CRM_Payu_Model_ActivityStatus::cancelled();
    CRM_Payu_Model_ActivityStatus::failed();
    CRM_Payu_Model_ActivityStatus::closed();

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_023_migrate_activity_statuses() {
    /* Deklaracja płatności cyklicznej Cancelled or Unreachable to Failed */
    $query = "UPDATE civicrm_activity
              SET status_id = %4
              WHERE activity_type_id = %1 AND status_id IN (%2, %3)";
    $params = [
      1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
      2 => [3, 'Integer'],
      3 => [5, 'Integer'],
      4 => [CRM_Payu_Model_ActivityStatus::failed(), 'Integer'],
    ];
    CRM_Core_DAO::executeQuery($query, $params);

    /* Deklaracja płatności cyklicznej Completed to ... */
    $query = "UPDATE civicrm_activity a
                JOIN civicrm_contribution_recur cr ON cr.id = a.source_record_id
              SET a.status_id = %4
              WHERE activity_type_id = %1 AND a.status_id = %2 AND cr.contribution_status_id = %3";
    $tabParams = [
      'Completed + Pending Recurring to New' => [
        1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
        2 => [2, 'Integer'],
        3 => [2, 'Integer'],
        4 => [CRM_Payu_Model_ActivityStatus::new(), 'Integer'],
      ],
      'Completed + Cancelled Recurring to Cancelled' => [
        1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
        2 => [2, 'Integer'],
        3 => [3, 'Integer'],
        4 => [CRM_Payu_Model_ActivityStatus::cancelled(), 'Integer'],
      ],
      'Completed + Failed Recurring to Failed' => [
        1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
        2 => [2, 'Integer'],
        3 => [4, 'Integer'],
        4 => [CRM_Payu_Model_ActivityStatus::failed(), 'Integer'],
      ],
      'Completed + In progress Recurring to Active' => [
        1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
        2 => [2, 'Integer'],
        3 => [5, 'Integer'],
        4 => [CRM_Payu_Model_ActivityStatus::active(), 'Integer'],
      ],
    ];
    foreach ($tabParams as $params) {
      CRM_Core_DAO::executeQuery($query, $params);
    }

    /* Status of Ważność karty kredytowej depends on Deklaracja płatności cyklicznej */
    $query = "UPDATE civicrm_activity a
                JOIN civicrm_contribution_recur cr ON cr.id = a.source_record_id
                JOIN civicrm_activity aw ON aw.source_record_id = cr.id
              SET aw.status_id = a.status_id
              WHERE a.activity_type_id = %1 AND aw.activity_type_id = %2";
    $params = [
      1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
      2 => [CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(), 'Integer'],
    ];
    CRM_Core_DAO::executeQuery($query, $params);

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_024_fix_target_field() {
    $query = "INSERT IGNORE INTO civicrm_activity_contact (activity_id, contact_id, record_type_id)
              SELECT ac.activity_id, ac.contact_id contact_added_id, 3
              FROM civicrm_activity a
              INNER JOIN civicrm_activity_contact ac ON ac.activity_id = a.id AND ac.record_type_id = 2
              WHERE a.activity_type_id IN (%1, %2) AND ac.activity_id NOT IN (
                  SELECT ac.activity_id
                  FROM civicrm_activity a
                    INNER JOIN civicrm_activity_contact ac ON ac.activity_id = a.id AND ac.record_type_id = 3
                  WHERE a.activity_type_id IN (%1, %2)
                  )";
    $params = [
      1 => [CRM_Payu_Model_ActivityType::deklaracjaPlatnosciCyklicznej(), 'Integer'],
      2 => [CRM_Payu_Model_ActivityType::waznoscKartyKredytowej(), 'Integer'],
    ];
    CRM_Core_DAO::executeQuery($query, $params);

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_025_add_history_extent() {
    CRM_Payu_Model_ActivityType::probaPobraniaDeklaracjiCyklicznej();

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_026_missing_option_values() {

    foreach (CRM_Payu_Model_CardStatus::$labels as $code => $info) {
      try{
        $exists = (bool)\Civi\Api4\OptionValue::get(FALSE)
          ->addWhere('option_group_id:name', '=', 'status_proby_pobrania')
          ->addWhere('value', '=', $code)
          ->execute()
          ->count();
        if(!$exists) {
          $name = $code . '_' . trim(preg_replace('/[^\w-]/', '_', $info), '_' );
          $name = preg_replace('/_+/', '_', $name);

          \Civi\Api4\OptionValue::create(TRUE)
            ->setLanguage('en_US')
            ->addValue('option_group_id.name', 'status_proby_pobrania')
            ->addValue('value', $code)
            ->addValue('name', $name)
            ->addValue('label', sprintf("%s - %s", $code, $info ))
            ->execute();
        }
      }catch(\Exception $e){}

    }

    return TRUE;
  }

  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_038_activity_custom_fields() {
    CRM_Payu_Model_ActivityType::deklaracjaPayUAnulowanie();
    CRM_Payu_Model_ActivityType::deklaracjaPayUBlad();
    CRM_Payu_Model_ActivityType::deklaracjaPayUZamkniecie();

    CRM_Payu_Model_Custom_Group::payUZdarzenie();

    CRM_Payu_Model_Custom_Field::statusPobrania();

    return TRUE;
  }
  /**
   * @return bool
   * @throws CiviCRM_API3_Exception
   */
  public function upgrade_050_add_activity_zmianaKwoty() {
    CRM_Payu_Model_ActivityType::zmianaStalejWplaty();

    return TRUE;
  }
  
}
