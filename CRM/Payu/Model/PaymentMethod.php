<?php

class CRM_Payu_Model_PaymentMethod extends CRM_Payu_Model {
  use CRM_Toolbox_PaymentMethod;

  private const PAYU = 'PayU';
  private const PAYU_ACCOUNT = 'PayU Account';

  /**
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  public static function payu(): int {
    return (int) self::set(__METHOD__, self::PAYU, [
      'financial_account' => [
        'name' => self::PAYU_ACCOUNT,
        'type' => 'Asset',
        'accounting_code' => 'PUTRX',
        'is_active' => 1,
        'is_tax' => FALSE,
        'is_deductible' => FALSE,
        'is_header_account' => FALSE,
      ]
    ]);
  }

}
