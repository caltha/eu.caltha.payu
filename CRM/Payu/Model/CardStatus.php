<?php

/**
 * Lista statusów transakcji kartowych odsyłana przez PayU.
 *
 * @link https://developers.payu.com/pl/restapi.html#references_card_statuses
 * Class CRM_Payu_Model_CardStatus
 */
class CRM_Payu_Model_CardStatus {

  const NOT_APPLICABLE = 'ND';
  const INVALID_TOKEN = 'INV';

  const OK_000 = '000';

  const REFER_TO_CARD_ISSUER_S01 = 'S01';
  const PICKUP_CARD_S04 = 'S04';
  const DO_NOT_HONOR_S05 = 'S05';
  const INVALID_TRANSACTION_S12 = 'S12';
  const INVALID_AMOUNT_S13 = 'S13';
  const MESSAGE_FORMAT_ERROR_S30 = 'S30';
  const STOLEN_CARD_S43 = 'S43';
  const INSUFFICIENT_FUND_S51 = 'S51';
  const EXPIRED_CARD_S54 = 'S54';
  const DISABLED_FOR_ECOMMERCE_S57 = 'S57';
  const EXCEEDS_APPROVAL_AMOUNT_S61 = 'S61';
  const RESTRICTED_CARD_S62 = 'S62';
  const EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT_S65 = 'S65';
  const DESTINATION_NOT_AVAILABLE_S90 = 'S90';
  const DISABLED_FOR_ECOMMERCE_TRANSACTIONS_S93 = 'S93';
  const AUTHORIZATION_ERROR_S99 = 'S99';
  const UNENABLE_TO_AUTHORIZE_SN0 = 'SN0';
  const ENTER_LESSER_AMOUNT_SP9 = 'SP9';
  const CARD_NOT_SUPPORT_ST3 = 'ST3';
  const CARD_INACTIVE_OR_CLOSED = 'ST5';
  const INVALID_ACCOUNT_ST8 = 'ST8';
  const SOFT_DECLINED_SSD = 'SSD';
  const NO_SUCH_ISSUER = 'SIN';
  const OVER_DAILY_LIMIT = 'SP1';
  const ACCOUNT_CLOSED = 'SAC';
  const POSSIBLE_FRAUD = 'SPF';
  const AUTHENTICATION_3DS_ERROR_114 = '114';
  const AUTHENTICATION_3DS_ERROR_115 = '115';
  const AUTHENTICATION_3DS_ERROR_116 = '116';
  const AUTHENTICATION_3DS_ERROR_117 = '117';
  const PROCESSING_3DS_ERROR_120 = '120';
  const MISSING_CRYPTOGRAM_TOKENS = '123';
  const NON_COMPLIANT_PREPAID_CARD = '130';
  const ISSUER_NEVER_APPROVE = '132';
  const TRANSACTION_NOT_RECEIVED_222 = '222';
  const TECH_001 = '001';
  const TECH_002 = '002';
  const TECH_003 = '003';
  const TECH_004 = '004';
  const TECH_005 = '005';
  const ANTYFRAUD_110 = '110';
  const ANTYFRAUD_111 = '111';
  const ANTYFRAUD_112 = '112';
  const ANTYFRAUD_113 = '113';
  const ANTYFRAUD_121 = '121';
  const ANTYFRAUD_122 = '122';
  const ANTYFRAUD_221 = '221';
  const ANTYFRAUD_223 = '223';

  /**
   * @var string[]
   */
  public static $active = [
    self::OK_000,
  ];

  /**
   * @var string[]
   */
  public static $cancel = [
    self::DO_NOT_HONOR_S05,
    self::PICKUP_CARD_S04,
    self::INVALID_TRANSACTION_S12,
    self::INVALID_AMOUNT_S13,
    self::INSUFFICIENT_FUND_S51,
    self::EXCEEDS_APPROVAL_AMOUNT_S61,
    self::OVER_DAILY_LIMIT,
  ];

  /**
   * @var string[]
   */
  public static $fail = [
    self::STOLEN_CARD_S43,
    self::RESTRICTED_CARD_S62,
    self::DISABLED_FOR_ECOMMERCE_S57,
    self::DISABLED_FOR_ECOMMERCE_TRANSACTIONS_S93,
    self::AUTHORIZATION_ERROR_S99,
    self::UNENABLE_TO_AUTHORIZE_SN0,
    self::CARD_NOT_SUPPORT_ST3,
    self::INVALID_ACCOUNT_ST8,
    self::SOFT_DECLINED_SSD,
    self::CARD_INACTIVE_OR_CLOSED ,
    self::AUTHENTICATION_3DS_ERROR_114,
    self::AUTHENTICATION_3DS_ERROR_115,
    self::AUTHENTICATION_3DS_ERROR_116,
    self::AUTHENTICATION_3DS_ERROR_117,
    self::PROCESSING_3DS_ERROR_120,
    self::MISSING_CRYPTOGRAM_TOKENS,
    self::NON_COMPLIANT_PREPAID_CARD,
    self::NO_SUCH_ISSUER,
    self::ACCOUNT_CLOSED,
    self::POSSIBLE_FRAUD,
    self::ISSUER_NEVER_APPROVE,
  ];

  /**
   * @var string[]
   */
  public static $close = [
    self::EXPIRED_CARD_S54,
  ];

  /**
   * @var string[]
   */
  public static $labels = [
    self::NOT_APPLICABLE => 'Not applicable',
    self::INVALID_TOKEN => 'Invalid Token',
    self::OK_000 => 'OK',
    self::REFER_TO_CARD_ISSUER_S01 => 'Refer to card issuer',
    self::PICKUP_CARD_S04 => 'Pickup card',
    self::DO_NOT_HONOR_S05 => 'Do not honor	',
    self::INVALID_TRANSACTION_S12 => 'Invalid transaction',
    self::INVALID_AMOUNT_S13 => 'Invalid amount',
    self::MESSAGE_FORMAT_ERROR_S30 => 'Message format error',
    self::STOLEN_CARD_S43 => 'Pickup card (stolen card)',
    self::INSUFFICIENT_FUND_S51 => 'Insufficient funds',
    self::EXPIRED_CARD_S54 => 'Expired card',
    self::DISABLED_FOR_ECOMMERCE_S57 => 'Card disabled for e-commerce or cross-border transactions',
    self::EXCEEDS_APPROVAL_AMOUNT_S61 => 'Exceeds approval amount',
    self::RESTRICTED_CARD_S62 => 'Restricted card / Country exclusion table',
    self::EXCEEDS_WITHDRAWAL_FREQUENCY_LIMIT_S65 => 'Exceeds withdrawal frequency limit',
    self::DESTINATION_NOT_AVAILABLE_S90 => 'Destination not available',
    self::DISABLED_FOR_ECOMMERCE_TRANSACTIONS_S93 => 'Card disabled for e-commerce transactions',
    self::AUTHORIZATION_ERROR_S99 => 'Authorization error – default',
    self::UNENABLE_TO_AUTHORIZE_SN0 => 'Unable to authorize / Force STIP',
    self::ENTER_LESSER_AMOUNT_SP9 => 'Enter lesser amount',
    self::CARD_NOT_SUPPORT_ST3 => 'Card not supported',
    self::CARD_INACTIVE_OR_CLOSED => 'Card inactive or closed (updated information needed)',
    self::INVALID_ACCOUNT_ST8 => 'Invalid account',
    self::SOFT_DECLINED_SSD => 'Soft decline (strong authentication required)',
    self::AUTHENTICATION_3DS_ERROR_114 => '3DS authentication error',
    self::AUTHENTICATION_3DS_ERROR_115 => '3DS authentication error',
    self::AUTHENTICATION_3DS_ERROR_116 => '3DS authentication error',
    self::AUTHENTICATION_3DS_ERROR_117 => '3DS authentication error',
    self::PROCESSING_3DS_ERROR_120 => '3ds processing error',
    self::MISSING_CRYPTOGRAM_TOKENS => 'Missing cryptogram for Network Token\'s authorization.',
    self::NON_COMPLIANT_PREPAID_CARD => 'Non-compliant prepaid card',
    self::ISSUER_NEVER_APPROVE => 'Issuer will never approve',
    self::NO_SUCH_ISSUER => 'No such issuer',
    self::OVER_DAILY_LIMIT => 'Over daily limit (try again later)',
    self::ACCOUNT_CLOSED => 'Account closed (do not try again)',
    self::POSSIBLE_FRAUD => 'Possible fraud (do not try again)',
    self::TRANSACTION_NOT_RECEIVED_222 => 'Transaction not received by merchant',
    self::TECH_001 => 'Techniczny',
    self::TECH_002 => 'Techniczny',
    self::TECH_003 => 'Techniczny',
    self::TECH_004 => 'Techniczny',
    self::TECH_005 => 'Techniczny',
    self::ANTYFRAUD_110 => 'Antyfrauding',
    self::ANTYFRAUD_111 => 'Antyfrauding',
    self::ANTYFRAUD_112 => 'Antyfrauding',
    self::ANTYFRAUD_113 => 'Antyfrauding',
    self::ANTYFRAUD_121 => 'Antyfrauding',
    self::ANTYFRAUD_122 => 'Antyfrauding',
    self::ANTYFRAUD_221 => 'Antyfrauding',
    self::ANTYFRAUD_223 => 'Antyfrauding',
  ];
}
