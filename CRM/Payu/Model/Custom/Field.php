<?php

class CRM_Payu_Model_Custom_Field {
  use CRM_Toolbox_CustomField;

  const STATUS_POBRANIA = 'status_pobrania';
  const TRANSAKCJA = 'transakcja';
  const SZCZEGOLY_TRANSAKCJI = 'Szczegóły transakcji';
  /**
   * @return string
   * @throws CiviCRM_API3_Exception
   */
  public static function statusPobrania(): string {
    return self::setSelectText(
      __METHOD__,
      self::STATUS_POBRANIA,
      CRM_Payu_Model_Custom_Group::payUZdarzenie(),
      [
        'label' => 'Status pobrania',
        'is_required' => 1,
        'is_searchable' => 1,
        'column_name' => self::STATUS_POBRANIA,
        'option_group_id' => \Civi\Api4\OptionGroup::get(FALSE)
          ->addSelect('id')
          ->addWhere('name', '=', 'status_proby_pobrania')
          ->execute()->single()['id'],
      ]
    );
  }

}
