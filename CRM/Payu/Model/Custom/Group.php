<?php

class CRM_Payu_Model_Custom_Group {
  use CRM_Toolbox_CustomGroup;

  const PAYU_ZDARZENIE = 'payu_zdarzenie';
  const PAYU_ZMIANAKWOTY = 'payu_zmianakwoty';
  /**
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  public static function payUZdarzenie(): int {
    return self::set(
      __METHOD__,
      self::PAYU_ZDARZENIE,
      [
        'title' => 'PayU Zdarzenie',
        'extends' => 'Activity',
        'extends_entity_column_value' => [
          CRM_Payu_Model_ActivityType::deklaracjaPayUAnulowanie(),
          CRM_Payu_Model_ActivityType::deklaracjaPayUBlad(),
          CRM_Payu_Model_ActivityType::deklaracjaPayUZamkniecie()
        ],
        'table_name' => 'civicrm_value_payu_zdarzenie',
        'is_active' => 1,
        'weight' => 1,
      ]
    );
  }
  
}
