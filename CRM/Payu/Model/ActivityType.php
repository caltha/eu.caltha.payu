<?php

class CRM_Payu_Model_ActivityType extends CRM_Payu_Model {

  const DEKLARACJA_PLATNOSCI_CYKLICZNEJ = 'Deklaracja płatności cyklicznej';
  const PROBA_POBRANIA_DEKLARACJI_CYKLICZNEJ = 'Próba pobrania deklaracji cyklicznej';
  const WAZNOSC_KARTY_KREDYTOWEJ = 'Ważność karty kredytowej';

  const DEKLARACJA_PAYU_CANCEL = 'Deklaracja PayU - anulowanie';
  const DEKLARACJA_PAYU_CLOSE = 'Deklaracja PayU - zamknięcie';
  const DEKLARACJA_PAYU_ERROR = 'Deklaracja PayU - błąd';
  const ZMIANA_STALEJ_WPLATY = 'Zmiana wysokości stałej wpłaty';
  /**
   * Get Id of Deklaracja PayU - błąd
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function deklaracjaPayUBlad() {
    return self::set(__FUNCTION__, self::DEKLARACJA_PAYU_ERROR);
  }

  /**
   * Get Id of Deklaracja PayU - zamknięcie
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function deklaracjaPayUZamkniecie() {
    return self::set(__FUNCTION__, self::DEKLARACJA_PAYU_CLOSE);
  }

  /**
   * Get Id of Deklaracja PayU - anulowanie
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function deklaracjaPayUAnulowanie() {
    return self::set(__FUNCTION__, self::DEKLARACJA_PAYU_CANCEL);
  }

  /**
   * Get Id of Deklaracja płatności cyklicznej
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function deklaracjaPlatnosciCyklicznej() {
    return self::set(__FUNCTION__, self::DEKLARACJA_PLATNOSCI_CYKLICZNEJ);
  }

  /**
   * Get activity type id of Próba pobrania deklaracji cyklicznej.
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function probaPobraniaDeklaracjiCyklicznej() {
    return self::set(__FUNCTION__, self::PROBA_POBRANIA_DEKLARACJI_CYKLICZNEJ);
  }

  /**
   * Get Id of Ważność karty kredytowej
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function waznoscKartyKredytowej() {
    return self::set(__FUNCTION__, self::WAZNOSC_KARTY_KREDYTOWEJ);
  }
/**
   * Get Id of Ważność karty kredytowej
   *
   * @return int|mixed
   * @throws \CiviCRM_API3_Exception
   */
  public static function zmianaStalejWplaty() {
    return self::set(__FUNCTION__, self::ZMIANA_STALEJ_WPLATY);
  }
  
  /**
   * @param $name
   *
   * @return int
   * @throws \CiviCRM_API3_Exception
   */
  private static function set($key, $name): int {
    $key = __CLASS__ . $key;
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = self::optionValue('activity_type', $name);
      Civi::cache()->set($key, $id);
      return $id;
    }
    return $cache;
  }

}
