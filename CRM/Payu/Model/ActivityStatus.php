<?php

class CRM_Payu_Model_ActivityStatus extends CRM_Payu_Model {

  const NEW = 'New';
  const ACTIVE = 'Active';
  const CANCELLED = 'Cancelled';
  const FAILED = 'Failed';
  const CLOSED = 'Closed';

  /**
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function new() {
    return self::set(__METHOD__, self::NEW, ['filter' => 1]);
  }

  /**
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function active() {
    return self::set(__METHOD__, self::ACTIVE, ['filter' => 1]);
  }

  /**
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function cancelled() {
    return self::set(__METHOD__, self::CANCELLED, ['filter' => 1]);
  }

  /**
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function failed() {
    return self::set(__METHOD__, self::FAILED, ['filter' => 1]);
  }

  /**
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  public static function closed() {
    return self::set(__METHOD__, self::CLOSED, ['filter' => 1]);
  }

  /**
   * Set activity status in cache and return id
   *
   * @param string $key
   * @param string $name
   * @param array $options
   *
   * @return int|mixed
   * @throws CiviCRM_API3_Exception
   */
  private static function set(string $key, string $name, array $options = []) {
    $cache = Civi::cache()->get($key);
    if (!isset($cache)) {
      $id = self::setOption($name, $options);
      Civi::cache()->set($key, $id);
      return $id;
    }

    return $cache;
  }

  /**
   * @param string $name
   * @param array $options
   *
   * @return int
   * @throws CiviCRM_API3_Exception
   */
  private static function setOption(string $name, array $options = []) {
    return self::optionValue('activity_status', $name, $options);
  }

}
