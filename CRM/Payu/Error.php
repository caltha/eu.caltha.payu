<?php

class CRM_Payu_Error {

  const PREFIX = '[PAYU-EXT] ';

  /**
   * @param string $variable_name
   * @param mixed $variable
   * @param string $context
   * @return string
   */
  public static function debug_var(string $variable_name, $variable, string $context = '') {
    $fullPrefix = $context ? self::PREFIX . $context : self::PREFIX;
    return CRM_Core_Error::debug_var($fullPrefix . $variable_name, $variable);
  }

  /**
   * @param string $message
   * @param string $context
   * @return string
   */
  public static function debug_log_message(string $message, string $context = '') {
    $fullPrefix = $context ? self::PREFIX . $context : self::PREFIX;
    return CRM_Core_Error::debug_log_message($fullPrefix . $message);
  }

}
