<?php
return [
  0 => [
    'name' => 'Payu',
    'entity' => 'PaymentProcessorType',
    'params' => [
      'version' => 3,
      'name' => 'Payu',
      'title' => 'Payu',
      'description' => 'Payu Payment Processor',
      'class_name' => 'Payment_Payu',
      'user_name_label' => 'Id punktu płatności (pos_id)',
      'password_label' => 'Drugi klucz (MD5)',
      'signature_label' => 'Protokół OAuth - client_id',
      'subject_label' => 'Protokół OAuth - client_secret',
      'url_site_default' => 'https://secure.payu.com',
      'url_site_test_default' => 'https://secure.snd.payu.com',
      'url_api_default' => 'https://secure.payu.com/api/v2_1/orders',
      'url_api_test_default' => 'https://secure.snd.payu.com/api/v2_1/orders',
      'billing_mode' => 4, // notify
      'is_default' => 0,
      'is_recur' => 1,
      'payment_type' => 1,
    ]
  ],
];
