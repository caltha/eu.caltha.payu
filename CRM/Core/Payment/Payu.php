<?php

require_once 'CRM/Core/Payment.php';
require_once __DIR__ . '/../../../packages/vendor/autoload.php';

class CRM_Core_Payment_Payu extends CRM_Core_Payment {

  /**
   * We only need one instance of this object. So we use the singleton
   * pattern and cache the instance in this variable
   *
   * @var object
   * @static
   */
  static private $_singleton = NULL;

  /**
   * Mode of operation: live or test.
   *
   * @var object
   */
  protected $_mode = NULL;

  public function __construct($mode, &$paymentProcessor) {
    $this->_mode = $mode;
    $this->_paymentProcessor = $paymentProcessor;
    $this->_processorName = ts('Payu');
  }

  /**
   * @param array|\Civi\Payment\PropertyBag $params
   * @param string $component
   *
   * @throws \CiviCRM_API3_Exception
   * @throws \OpenPayU_Exception
   * @throws \OpenPayU_Exception_Configuration
   */
  public function doPayment(&$params, $component = 'contribute') {
    $propertyBag = \Civi\Payment\PropertyBag::cast($params);
    CRM_Payu_Config::init($this->_paymentProcessor);
    if ($propertyBag->getIsRecur()) {
      $this->recurring($propertyBag, $this->_paymentProcessor);
    }
    $this->single($propertyBag, $this->_paymentProcessor);
  }

  /**
   * This function checks to see if we have the right config values.
   *
   * @return string
   *   the error message if any
   */
  function checkConfig() {
    $error = array();
    if (empty($this->_paymentProcessor['user_name'])) {
      $error[] = ts('Id punktu płatności (pos_id) is not set in the Administer &raquo; System Settings &raquo; Payment Processors');
    }
    if (empty($this->_paymentProcessor['password'])) {
      $error[] = ts('Second key is not set in the Administer &raquo; System Settings &raquo; Payment Processors');
    }
    if (!empty($error)) {
      return implode('<p>', $error);
    }
    else {
      return NULL;
    }
  }


  /**
   * Redirect to Dotpay page with data in POST.
   *
   * @param string $url
   * @param array $data
   */
  private function redirectPost($url, array $data) {
    echo '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <script type="text/javascript">
        function closethisasap() {
          document.forms["redirectpost"].submit();
        }
      </script>
    </head>
    <body onload="closethisasap();">
    <form name="redirectpost" method="post" action="' . $url . '">';
    if (!is_null($data)) {
      foreach ($data as $k => $v) {
        echo '<input type="hidden" name="' . $k . '" value="' . $v . '"> ';
      }
    }
    echo '</form>
    </body>
    </html>';
    CRM_Utils_System::civiExit();
  }

  /**
   * @param \Civi\Payment\PropertyBag $propertyBag
   * @param array $paymentProcessor
   */
  private function recurring($propertyBag, $paymentProcessor) {
    $form = CRM_Payu_Logic_Form::prepare($propertyBag, $paymentProcessor);
    $signature = CRM_Payu_Logic_Form::signature($form);
    $form = CRM_Payu_Logic_Form::extend($form, $propertyBag, $signature, CRM_Payu_Config::$urlSite);
    $url = CRM_Utils_System::url('civicrm/payu/first', [], TRUE);
    $this->redirectPost($url, $form);
  }

  /**
   * @param \Civi\Payment\PropertyBag $params
   * @param array $paymentProcessor
   *
   * @throws \OpenPayU_Exception
   * @throws \CiviCRM_API3_Exception
   */
  private function single($propertyBag, $paymentProcessor) {
    $single = new CRM_Payu_Logic_Order_Single();
    $data = $single->find($propertyBag->getContributionID());
    $orderParams = $single->prepare($paymentProcessor['id'], OpenPayU_Configuration::getMerchantPosId(), $propertyBag->getContributionID(), $data);
    $order = OpenPayU_Order::create($orderParams);
    $status = $order->getStatus();
    $response = $order->getResponse();
    $redirectUri = $response->redirectUri;
    if ($status == CRM_Payu_Logic_Response::SUCCESS) {
      $single->saveOrderId($propertyBag->getContributionID(), $response->orderId);
      CRM_Utils_System::redirect($redirectUri);
    }
    // todo obsłużyć inne statusy? Jakie są?
  }

}
