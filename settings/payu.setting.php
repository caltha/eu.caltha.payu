<?php

return [
  'payu_interval_days' => [
    'name' => 'payu_interval_days',
    'settings_pages' => ['payu' => ['weight' => 10]],
    'type' => 'Integer',
    'quick_form_type' => 'Element',
    'html_type' => 'text',
    'default' => 1,
    'add' => '5.24',
    'is_domain' => 1,
    'is_contact' => 0,
    'title' => 'Interval in days between attempts of debiting',
    'description' => 'Interval in days between attempts of debiting',
    'help_text' => 'Interval in days between attempts of debiting, 1st and 2nd, 2nd and 3rd',
  ],
  'payu_extend_history' => [
    'name' => 'payu_extend_history',
    'title' => 'Extend history',
    'description' => 'Decide whether each attempts have activity "Próba pobrania deklaracji cyklicznej"',
    'type' => 'Integer',
    'html_type' => 'radio',
    'options' => [0 => 'No', 1 => 'Yes, each attempts have activity'],
    'default' => 0,
    'add' => '5.29',
    'is_domain' => 1,
    'is_contact' => 0,
    'settings_pages' => ['payu' => ['weight' => 20]],
  ],
];
